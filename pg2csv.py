print('*******************************************************************************')
print('*******************************************************************************')
print('*******************************************************************************')
print('Begin script.')

# Import modules
import argparse # Parse CLI args
import csv
from datetime import datetime
import openpyxl # Library used to create report file
from openpyxl import Workbook
import pandas as pd
import psycopg2 # PostgreSQL driver
import sfgis_toolbox # Reusable routines by SFGIS
import sqlalchemy # SQL ORM  # pip install sqlalchemy
from sqlalchemy import create_engine
import sys

# Start script timer
start = datetime.now()
print(sys.argv[0] + 'start: {0}'.format(start))

# ######################################################################################
# Read command line arguments
# ######################################################################################
argparser=argparse.ArgumentParser()

# Define required database connection args
argparser.add_argument('--odbc_server', help='Database Server', type=str)
argparser.add_argument('--odbc_port',  help='Database Port', type=str)
argparser.add_argument('--odbc_database', help='Database Name', type=str)
argparser.add_argument('--odbc_uid', help='User Name', type=str)
argparser.add_argument('--odbc_pwd', help='User Password', type=str)

# Define I/O args
argparser.add_argument('--input_table', help='Input table', type=str)
argparser.add_argument('--output_file', help='Output file (CSV)', type=str)
argparser.add_argument('--where_clause', help='Optional WHERE clause to filter input. Lead the clause with "AND"', default='', type=str)
argparser.add_argument('--limit', help='Limit records. Set to -1 for all records.', type=int, default=0)

# Parse args
args=argparser.parse_args()

# Store args as local variables
odbc_server = args.odbc_server
odbc_port = args.odbc_port
odbc_database = args.odbc_database
odbc_uid = args.odbc_uid
odbc_pwd = args.odbc_pwd
input_table = args.input_table
output_file = args.output_file
where_clause = args.where_clause
limit = args.limit

# Print usage and halt if missing any required arguments
if any(elem is None for elem in [odbc_server, odbc_port, odbc_database, odbc_uid, odbc_pwd, input_table, output_file]):
    print(argparser.format_help())
    exit()

# ######################################################################################
# Define database connection info
# ######################################################################################
odbc_driver = '{psqlodbc}'

connection = None

# ######################################################################################
# Define query limits
# ######################################################################################
if args.limit == -1:
    limit_source = ''
    limit_message = 'All records'
elif args.limit == 0:
    limit_source = ' LIMIT 0'
    limit_message = 'No records'
elif args.limit == 1:
    limit_source = ' LIMIT ' + format(limit+1) # Add 1 because the postgresql limit is a ceiling
    limit_message = '1 record'
elif args.limit > 1:
    limit_source = ' LIMIT ' + format(limit+1) # Add 1 because the postgresql limit is a ceiling
    limit_message = format(limit) + ' records'
else:
    print('Error. Invalid value for limit: {0}'.format(limit))
    exit()

# ######################################################################################
# Connect to database
# ######################################################################################

try:
    connection = psycopg2.connect(host=odbc_server, database=odbc_database, user=odbc_uid, password=odbc_pwd)
    cur = connection.cursor()

    # # ######################################################################################
    # # Export the input table to the output csv
    # # ######################################################################################
    engine = create_engine('postgresql+psycopg2://'+odbc_uid+':'+odbc_pwd+'@'+odbc_server+':'+odbc_port+'/'+odbc_database)

    # ------------------------------------------------------------------------------
    # # TODO: query optional records
    # ------------------------------------------------------------------------------

    # ------------------------------------------------------------------------------
    # Query and write distinct address records WITH UNIT
    # ------------------------------------------------------------------------------
    # sql = 'SELECT DISTINCT ON (delivery_address_with_unit) delivery_address_with_unit AS address_without_zip, concat(delivery_address_with_unit::text, ' ', zip_code::text) AS address_with_zip ,  * FROM ' + input_table

    sql = 'SELECT * FROM ' + input_table
    sql += ' WHERE 1=1 '
    sql += ' ' + where_clause
    sql += ' ORDER BY sfgisgapid '
    sql += limit_source

    print('sql: {0}'.format(sql))

    df = pd.read_sql_query(sql,con=engine)
    df.to_csv(output_file, quoting=csv.QUOTE_NONNUMERIC, index=False)

    # #------------------------------------------------------------------------------
    # # Dump the input table
    # #------------------------------------------------------------------------------
    print('-  -  -  -  -  -  -  -  -  -  -')
    sql = 'SELECT * FROM ' + input_table
    sql += ' WHERE 1=1 '
    sql += ' ' + where_clause
    cur.execute(sql)
    rows = cur.fetchall()
    print('Input Record Count: ' + str(len(rows)))

    colnames = [desc[0] for desc in cur.description]
    print('Input Columns: {0}'.format(colnames))

    sql = 'SELECT * FROM ' + input_table
    sql += ' WHERE 1=1 '
    sql += ' ' + where_clause
    sql += ' LIMIT 1'

    print('Input SQL: ' + sql)
    cur.execute(sql)

    rows = cur.fetchall()
    for row in rows:
        print('Row: {0}'.format(row))

finally:
    if connection:
        connection.close()

sfgis_toolbox.print_run_time(start)

print('*******************************************************************************')
print('                                                                               ')
print('End of script ' + sys.argv[0])
print('                                                                               ')
