print('*******************************************************************************')
print('*******************************************************************************')
print('*******************************************************************************')
print('Begin script.')

# ######################################################################################
# Import modules
# ######################################################################################
import argparse # Parse CLI args
from datetime import datetime
import openpyxl # Library used to create report file
from openpyxl import Workbook
import psycopg2 # PostgreSQL driver
import re       #regular expressions
import sfgis_toolbox # Reusable routines by SFGIS
import sys
import usaddress # Parserator 'usaddress' parsing module # See https://parserator.datamade.us/api-docs

# ######################################################################################
# Start script timer
# ######################################################################################
start = datetime.now()
print(sys.argv[0] + ' start: {0}'.format(start))

# ######################################################################################
# Read command line arguments
# ######################################################################################
argparser=argparse.ArgumentParser()

# Required database connection args
argparser.add_argument('--odbc_server', help='Database Server', type=str)
argparser.add_argument('--odbc_port',  help='Database Port', type=str)
argparser.add_argument('--odbc_database', help='Database Name', type=str)
argparser.add_argument('--odbc_uid', help='User Name', type=str)
argparser.add_argument('--odbc_pwd', help='User Password', type=str)

# Required args
argparser.add_argument('--address_table', help='Input address table', type=str)
argparser.add_argument('--output_table', help='Output address table (with P.O. Boxes removed)', type=str)
argparser.add_argument('--pobox_records_table_name', help='Output table listing P.O. Boxes', type=str)
argparser.add_argument('--output_report_file', help='Output report file (CSV or XLSX)', type=str)

# Optional args

# Parse args
args=argparser.parse_args()

# Store args as local variables
odbc_server = args.odbc_server
odbc_port = args.odbc_port
odbc_database = args.odbc_database
odbc_uid = args.odbc_uid
odbc_pwd = args.odbc_pwd
address_table = args.address_table
output_table = args.output_table
output_report_file = args.output_report_file
pobox_records_table_name = args.pobox_records_table_name

# Print usage and halt if missing any required arguments
if any(elem is None for elem in [odbc_server, odbc_port, odbc_database, odbc_uid, odbc_pwd, address_table, output_table, pobox_records_table_name, output_report_file]):
    print(argparser.format_help())
    exit()

# ######################################################################################
# Define database connection info
# ######################################################################################
odbc_driver = '{psqlodbc}'

connection = None

# ######################################################################################
# Connect to database
# ######################################################################################

try:
    connection = psycopg2.connect(host=odbc_server, database=odbc_database, user=odbc_uid, password=odbc_pwd)

    cur = connection.cursor()

    # ######################################################################################
    # Drop pre-existing P.O. Boxes table
    # ######################################################################################
    sql = 'DROP TABLE IF EXISTS ' + output_table
    cur.execute(sql)
    sql = 'DROP TABLE IF EXISTS ' + pobox_records_table_name
    cur.execute(sql)
    connection.commit()

    print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
    print('Report A: Input/Output Counts')
    print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
    # ######################################################################################
    # Get total record count
    # ######################################################################################
    sql = 'SELECT COUNT(*) AS count FROM ' + address_table
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    input_record_count = row[0]
    print('input_record_count: {0}'.format(input_record_count)) # should be 426929

    # ######################################################################################
    # Save copy without P.O. Boxes
    # ######################################################################################

    sql = 'CREATE TABLE ' + output_table + ' AS SELECT * FROM ' + address_table
    sql += " WHERE NOT slug LIKE 'BOXHOLDER'"
    cur.execute(sql)
    connection.commit()
    # Echo record count
    sql = 'SELECT COUNT(*) AS count FROM ' + output_table
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    output_table_count = row[0]
    print('output_table_count: {0}'.format(output_table_count)) # 408435

    # ######################################################################################
    # Calculate and report the difference in records after removing P.O. Boxes
    # ######################################################################################
    records_excluded = input_record_count - output_table_count
    print('Record excluded (input-output): {0}'.format(records_excluded)) # 18494

    print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
    print('Report B: P.O. Box Records/Addresses Counts')
    print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')

    # ######################################################################################
    # Save pobox records (pobox_records_table_name)
    # ######################################################################################

    # Save all pobox records to a new table
    sql = 'CREATE TABLE ' + pobox_records_table_name + ' AS SELECT * FROM ' + address_table
    sql += " WHERE slug LIKE 'BOXHOLDER'"
    cur.execute(sql)
    connection.commit()
    # Echo record count
    sql = 'SELECT COUNT(*) AS count FROM ' + pobox_records_table_name
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    pobox_record_count = row[0]
    print('{0} records: {1}'.format(pobox_records_table_name, pobox_record_count)) # 18494

    # ######################################################################################
    # Get distinct address records: concat address and slug
    # ######################################################################################
    # sql = 'SELECT DISTINCT ON (slug) * FROM ' + address_table
    # cur.execute(sql)
    # rows = cur.fetchall()
    # unique_count = len(rows)
    # print('unique_count (slug column): {0}'.format(unique_count)) #3
    # for row in rows:
    #     print('row: {0}'.format(row))
    #     # BOXHOLDER
    #     # BUSINESS OCCUPANT
    #     # RESIDENT

finally:
    status = 'complete'

# ######################################################################################
# Calculate run time 
# ######################################################################################
# Calculate processing time, time per recs ,  recs per time
end = datetime.now()
elapased = end - start
# Convert elapsed time into seconds
elapased_seconds = sfgis_toolbox.get_sec(format(elapased))

# ######################################################################################
# Save report
# ######################################################################################
wb = Workbook()

sheet_index = 0

# Record Count Sheet
ws1 = wb.create_sheet('Record Count Summary', sheet_index)
row = 0

# Script name
row += 1
col = 1
ws1.cell(row=row, column=col, value='Script name:')
col = 2
ws1.cell(row=row, column=col, value=format(sys.argv[0]))

# Script start time
row += 1
col = 1
ws1.cell(row=row, column=col, value='Script start time:')
col = 2
ws1.cell(row=row, column=col, value=format(start))

# Script end time
row += 1
col = 1
ws1.cell(row=row, column=col, value='Script end time:')
col = 2
ws1.cell(row=row, column=col, value=format(end))

# Script run time
row += 1
col = 1
ws1.cell(row=row, column=col, value='Script run time:')
col = 2
ws1.cell(row=row, column=col, value=format(elapased))

# Break
row += 1

# Input Table
row += 1
col = 1
ws1.cell(row=row, column=col, value='Input Table of Addresses:')
col = 2
ws1.cell(row=row, column=col, value=format(address_table))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Input Record Count:')
col = 2
ws1.cell(row=row, column=col, value=format(input_record_count))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Output Record Count:')
col = 2
ws1.cell(row=row, column=col, value=format(output_table_count))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Record Excluded  (input - output):')
col = 2
ws1.cell(row=row, column=col, value=format(records_excluded))

# TODO: add all that other stuff

row += 1
col = 1
ws1.cell(row=row, column=col, value='P.O. Box Record Count:')
col = 2
ws1.cell(row=row, column=col, value=format(pobox_record_count))

# Break
row += 1

# End of sheet
row += 1
col = 1
ws1.cell(row=row, column=col, value='End of sheet')

# TODO: Turn into a function and call for all sheets
# Adjust column widths
# See also https://stackoverflow.com/questions/13197574/openpyxl-adjust-column-width-size
dims = {}
for row in ws1.rows:
    for cell in row:
        if cell.value:
            dims[cell.column] = max((dims.get(cell.column, 0), len(cell.value)))

for col, value in dims.items():
    ws1.column_dimensions[col].width = value

# Save report
wb.save(output_report_file)

# Close database connection
if connection:
    connection.close()

# ######################################################################################
# Display results
# ######################################################################################

print('                                                                               ')
print('===============================================================================')
print('                                                                               ')
print(sys.argv[0])
print('                                                                               ')
# print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
# print('                                                                               ')
# print ('args: {0}'.format(args)) #TODO Filter out password and/or all odbc values before outputing
# print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('Input Table of Addresses: {0}'.format(address_table))
print('Input Record Count: {0}'.format(input_record_count))
print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('** Output Report File **')
print('                                                                               ')
print(format(output_report_file))
print('                                                                               ')
print('** Output Report Sheets **')
print('                                                                               ')
for sheet in wb:
    print(format(sheet.title))
print('                                                                               ')

sfgis_toolbox.print_run_time(start)

print('*******************************************************************************')
print('                                                                               ')
print('End of script ' + sys.argv[0])
print('                                                                               ')
