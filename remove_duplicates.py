print('*******************************************************************************')
print('*******************************************************************************')
print('*******************************************************************************')
print('Begin script.')

# ######################################################################################
# Import modules
# ######################################################################################
import argparse # Parse CLI args
from datetime import datetime
import openpyxl # Library used to create report file
from openpyxl import Workbook
import psycopg2 # PostgreSQL driver
import re       #regular expressions
import sfgis_toolbox # Reusable routines by SFGIS
import sys
import usaddress # Parserator 'usaddress' parsing module # See https://parserator.datamade.us/api-docs

# ######################################################################################
# Start script timer
# ######################################################################################
start = datetime.now()
print(sys.argv[0] + ' start: {0}'.format(start))

# ######################################################################################
# Read command line arguments
# ######################################################################################
argparser=argparse.ArgumentParser()

# Required database connection args
argparser.add_argument('--odbc_server', help='Database Server', type=str)
argparser.add_argument('--odbc_port',  help='Database Port', type=str)
argparser.add_argument('--odbc_database', help='Database Name', type=str)
argparser.add_argument('--odbc_uid', help='User Name', type=str)
argparser.add_argument('--odbc_pwd', help='User Password', type=str)

# Required args
argparser.add_argument('--address_table', help='Input address table', type=str)
argparser.add_argument('--output_table', help='Output address table (with duplicates removed)', type=str)
argparser.add_argument('--duplicate_records_table_name', help='Output table listing duplicate address records', type=str)
argparser.add_argument('--duplicate_addresses_table_name', help='Output table listing duplicate address counts', type=str)
argparser.add_argument('--output_report_file', help='Output report file (CSV or XLSX)', type=str)

# Optional args

# Parse args
args=argparser.parse_args()

# Store args as local variables
odbc_server = args.odbc_server
odbc_port = args.odbc_port
odbc_database = args.odbc_database
odbc_uid = args.odbc_uid
odbc_pwd = args.odbc_pwd
address_table = args.address_table
output_table = args.output_table
output_report_file = args.output_report_file
duplicate_records_table_name = args.duplicate_records_table_name
duplicate_addresses_table_name = args.duplicate_addresses_table_name

# Print usage and halt if missing any required arguments
if any(elem is None for elem in [odbc_server, odbc_port, odbc_database, odbc_uid, odbc_pwd, address_table, output_table, duplicate_records_table_name, duplicate_addresses_table_name, output_report_file]):
    print(argparser.format_help())
    exit()

# ######################################################################################
# Define database connection info
# ######################################################################################
odbc_driver = '{psqlodbc}'

connection = None

# ######################################################################################
# Connect to database
# ######################################################################################

try:
    connection = psycopg2.connect(host=odbc_server, database=odbc_database, user=odbc_uid, password=odbc_pwd)

    cur = connection.cursor()

    # ######################################################################################
    # Drop pre-existing duplicates table
    # ######################################################################################
    sql = 'DROP TABLE IF EXISTS ' + output_table
    cur.execute(sql)
    sql = 'DROP TABLE IF EXISTS ' + duplicate_records_table_name
    cur.execute(sql)
    sql = 'DROP TABLE IF EXISTS ' + duplicate_addresses_table_name
    cur.execute(sql)
    connection.commit()

    print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
    print('Report A: Input/Output Counts')
    print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
    # ######################################################################################
    # Get total record count
    # ######################################################################################
    sql = 'SELECT COUNT(*) AS count FROM ' + address_table
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    input_record_count = row[0]
    print('input_record_count: {0}'.format(input_record_count)) # 434480

    # ######################################################################################
    # Save copy without duplicates
    # ######################################################################################
    sql = 'CREATE TABLE ' + output_table + ' AS SELECT DISTINCT ON (address) * FROM ' + address_table
    cur.execute(sql)
    connection.commit()
    # Echo record count
    sql = 'SELECT COUNT(*) AS count FROM ' + output_table
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    output_table_count = row[0]
    print('output_table_count: {0}'.format(output_table_count)) # 426929

    # ######################################################################################
    # Calculate and report the difference in records after removing duplicates
    # ######################################################################################
    records_excluded = input_record_count - output_table_count
    print('Record excluded (input-output): {0}'.format(records_excluded)) # 7551

    print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
    print('Report B: Duplicate Records/Addresses Counts')
    print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')

    # ######################################################################################
    # Save duplicate records (duplicate_records_table_name)
    # ######################################################################################

    # Save all duplicate records to a new table
    sql = 'CREATE TABLE ' + duplicate_records_table_name + ' AS SELECT * FROM (SELECT *, COUNT(*) over (PARTITION BY address) as count from ' + address_table + ') t WHERE count > 1'
    cur.execute(sql)
    connection.commit()
    # Echo record count
    sql = 'SELECT COUNT(*) AS count FROM ' + duplicate_records_table_name
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    duplicate_record_count = row[0]
    print('{0} records: {1}'.format(duplicate_records_table_name, duplicate_record_count)) # 8571

    # ######################################################################################
    # Save duplicate address counts (duplicate_addresses_table_name)
    # ######################################################################################

    # Save duplicate address names (not all the records) to new table.
    sql = 'CREATE TABLE  ' + duplicate_addresses_table_name + ' AS SELECT address, COUNT(address) AS COUNT FROM ' + address_table + ' GROUP BY address HAVING COUNT(*) > 1 ORDER BY COUNT DESC'
    cur.execute(sql)
    connection.commit()
    # Echo record count
    sql = 'SELECT COUNT(*) AS count FROM ' + duplicate_addresses_table_name
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    duplicate_address_count = row[0]
    print('{0} records: {1}'.format(duplicate_addresses_table_name, duplicate_address_count)) # 1020


    # ######################################################################################
    # Calculate and report the difference duplicate records vs addresses
    # ######################################################################################
    records_excluded2 = duplicate_record_count - duplicate_address_count
    print('Record excluded (duplicate records - duplicate addresses): {0}'.format(records_excluded2)) # 7551 



    print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
    print('Report C: Sum of duplicate_address_count should be same total as {0}'.format(duplicate_records_table_name))
    print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')

    # Get sum of all counts in duplicate_records_table_name: goal 8571
    sql = 'SELECT SUM(count) AS sum_count FROM ' + duplicate_addresses_table_name
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    dup_record_count = row[0]
    print('duplicate_records_table_name (sum count): {0}'.format(dup_record_count)) # 8571

    print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
    print('Report D: Final cross check')
    print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
    # ######################################################################################
    # Cross check duplicate totals
    # ######################################################################################
    sql = 'SELECT address, COUNT(address) AS COUNT FROM ' + address_table + ' GROUP BY address HAVING COUNT(*) > 1 ORDER BY COUNT DESC'
    cur.execute(sql)
    rows = cur.fetchall()
    duplicate_address_count = len(rows)
    print('duplicate_address_count (cross-check count > 1): {0}'.format(duplicate_address_count)) #1020


    # TODO: REMOVE THE FOLLOWING INTO remove_poboxes.py
    # ######################################################################################
    # Get distinct address records: concat address and slug
    # ######################################################################################
    sql = 'SELECT DISTINCT ON (slug) * FROM ' + address_table
    cur.execute(sql)
    rows = cur.fetchall()
    unique_count = len(rows)
    print('unique_count (slug column): {0}'.format(unique_count)) #3
    #for row in rows:
        #print('row: {0}'.format(row))

    sql = 'SELECT DISTINCT address, slug FROM ' + address_table
    cur.execute(sql)
    rows = cur.fetchall()
    unique_count = len(rows)
    print('unique_count (address + slug column): {0}'.format(unique_count)) #426929


    # ######################################################################################
    # Get list of address that are both residentail and biz
    # ######################################################################################
    # BOXHOLDER
    # BUSINESS OCCUPANT
    # RESIDENT

    # TODO: use the correct logic.
    # might need to alias two table (same table) and do where query

    # #sql = 'SELECT address, slug FROM ' + address_table
    # sql = 'SELECT address FROM ' + address_table
    # sql += " WHERE slug = 'BUSINESS OCCUPANT'"
    # sql = ' INTERSECT '
    # #sql = 'SELECT address, slug FROM ' + address_table
    # sql = 'SELECT address FROM ' + address_table
    # sql += " WHERE slug = 'RESIDENT'"
    # cur.execute(sql)
    # rows = cur.fetchall()
    # unique_count = len(rows)
    # print('TODO cross check unique_count (union): {0}'.format(unique_count)) #373785
    # #for row in rows:
    #     #print('row: {0}'.format(row))


finally:
    status = 'complete'

# ######################################################################################
# Calculate run time 
# ######################################################################################
# Calculate processing time, time per recs ,  recs per time
end = datetime.now()
elapased = end - start
# Convert elapsed time into seconds
elapased_seconds = sfgis_toolbox.get_sec(format(elapased))

# ######################################################################################
# Save report
# ######################################################################################
wb = Workbook()

sheet_index = 0

# Record Count Sheet
ws1 = wb.create_sheet('Record Count Summary', sheet_index)
row = 0

# Script name
row += 1
col = 1
ws1.cell(row=row, column=col, value='Script name:')
col = 2
ws1.cell(row=row, column=col, value=format(sys.argv[0]))

# Script start time
row += 1
col = 1
ws1.cell(row=row, column=col, value='Script start time:')
col = 2
ws1.cell(row=row, column=col, value=format(start))

# Script end time
row += 1
col = 1
ws1.cell(row=row, column=col, value='Script end time:')
col = 2
ws1.cell(row=row, column=col, value=format(end))

# Script run time
row += 1
col = 1
ws1.cell(row=row, column=col, value='Script run time:')
col = 2
ws1.cell(row=row, column=col, value=format(elapased))

# Break
row += 1

# Input Table
row += 1
col = 1
ws1.cell(row=row, column=col, value='Input Table of Addresses:')
col = 2
ws1.cell(row=row, column=col, value=format(address_table))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Input Record Count:')
col = 2
ws1.cell(row=row, column=col, value=format(input_record_count))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Output Record Count:')
col = 2
ws1.cell(row=row, column=col, value=format(output_table_count))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Record Excluded  (input - output):')
col = 2
ws1.cell(row=row, column=col, value=format(records_excluded))

# TODO: add all that other stuff

row += 1
col = 1
ws1.cell(row=row, column=col, value='Duplicate Record Count:')
col = 2
ws1.cell(row=row, column=col, value=format(duplicate_record_count))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Duplicate Address Count:')
col = 2
ws1.cell(row=row, column=col, value=format(duplicate_address_count))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Record Excluded (duplicate records - duplicate addresses):')
col = 2
ws1.cell(row=row, column=col, value=format(records_excluded2))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Duplicate Records cross check (sum count of duplicate addresses):')
col = 2
ws1.cell(row=row, column=col, value=format(dup_record_count))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Duplicate Addresses cross check (query where count > 1):')
col = 2
ws1.cell(row=row, column=col, value=format(duplicate_address_count))

# Break
row += 1

# End of sheet
row += 1
col = 1
ws1.cell(row=row, column=col, value='End of sheet')

# TODO: Turn into a function and call for all sheets
# Adjust column widths
# See also https://stackoverflow.com/questions/13197574/openpyxl-adjust-column-width-size
dims = {}
for row in ws1.rows:
    for cell in row:
        if cell.value:
            dims[cell.column] = max((dims.get(cell.column, 0), len(cell.value)))

for col, value in dims.items():
    ws1.column_dimensions[col].width = value

def write_table_records(table_name, sheet_index, wb, cur):
    sheet_index += 1
    ws = wb.create_sheet(table_name + ' Records')
    row = 0

    # Query the records
    sql = 'SELECT address, count FROM ' + table_name + ' ORDER BY count DESC'

    cur.execute(sql)

    # Echo the table name
    row += 1
    col = 1
    ws.cell(row=row, column=col, value='Table:')
    col = 2
    ws.cell(row=row, column=col, value=format(table_name))

    # Break
    row += 1

    # Break
    row += 1

    # Write Header
    row += 1
    col = 1
    ws.cell(row=row, column=col, value='ADDRESS')
    col = 2
    ws.cell(row=row, column=col, value='COUNT')

    # Write Rows
    rows = cur.fetchall()
    for r in rows:
        row += 1
        col = 1
        ws.cell(row=row, column=col, value=format(r[0]))
        col = 2
        ws.cell(row=row, column=col, value=format(r[1]))

write_table_records(duplicate_addresses_table_name, sheet_index, wb, cur)

# Save report
wb.save(output_report_file)

# Close database connection
if connection:
    connection.close()

# ######################################################################################
# Display results
# ######################################################################################

print('                                                                               ')
print('===============================================================================')
print('                                                                               ')
print(sys.argv[0])
print('                                                                               ')
# print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
# print('                                                                               ')
# print ('args: {0}'.format(args)) #TODO Filter out password and/or all odbc values before outputing
# print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('Input Table of Addresses: {0}'.format(address_table))
print('Input Record Count: {0}'.format(input_record_count))

print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('** Output Report File **')
print('                                                                               ')
print(format(output_report_file))
print('                                                                               ')
print('** Output Report Sheets **')
print('                                                                               ')
for sheet in wb:
    print(format(sheet.title))
print('                                                                               ')

sfgis_toolbox.print_run_time(start)

print('*******************************************************************************')
print('                                                                               ')
print('End of script ' + sys.argv[0])
print('                                                                               ')
