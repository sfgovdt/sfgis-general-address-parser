# IMPORT MODULES
import numbers
import re # regular expressions

# LOCAL VARIABLES

# FUNCTIONS

def get_parity(i_integer):
    """ Returns parity of an integer: 'even', 'odd', or '' if neither () """
    if (is_even(i_integer)):
        parity = 'even'
    elif (is_odd(i_integer)):
        parity = 'odd'
    else:
        parity = ''
    return parity

def get_sec(time_str):
    """ Converts time to seconds """
    # See https://stackoverflow.com/questions/6402812/how-to-convert-an-hmmss-time-string-to-seconds-in-python/6402934
    # See https://stackoverflow.com/questions/1841565/valueerror-invalid-literal-for-int-with-base-10?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

    h, m, s = time_str.split(':')

    # Note: Convert seconds to float to handle fractions of a second in the time passed in
    return int(h) * 3600 + int(m) * 60 + float(s)
    #return int(h) * 3600 + int(m) * 60 + int(s)

def is_even(i_integer):
    " Return true if integer is even "
    return (i_integer % 2 == 0)

def is_odd(i_integer):
    """ Return true if integer is odd """
    return (i_integer % 2 != 0)

def print_run_time(start):
    """ Prints script run time based on start time (arg='start') and current time. """
    from datetime import datetime
    end = datetime.now()
    elapased = end - start
    print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
    print('                                                                               ')
    print('** Script processing time **')
    print('                                                                               ')
    print('Script start time: {0}'.format(start))
    print('Script end time:   {0}'.format(end))
    print('Script run time:               {0}'.format(elapased))
    print('                                                                               ')

def remove_duplicate_spaces(input_string):
    """ Eliminate leading, trailing and duplicate spaces in string """
    # See https://stackoverflow.com/questions/1546226/simple-way-to-remove-multiple-spaces-in-a-string
    # Convert to string if not already
    input_string = str(input_string)

    return re.sub(r' {2,}' , ' ', input_string).strip() #Also strip leading and trailing spaces

def validate_integer(x):
    """ Ensure the input is an integer """
    if isinstance(x,numbers.Number):
        return x
    for tpe in (int, float):
        try:
            return tpe(x)
        except ValueError:
            continue
    return 0
