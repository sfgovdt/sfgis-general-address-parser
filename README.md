# SFGIS General Address Parser

The SFGIS *General Address Parser* is a set of stand-alone Python routines that import, parse, standardize, validate and export San Francisco addresses. 

The validation step answers the questions "Is this address in the EAS?", "Does the address have a legal San Francisco complete street name?", and "If the complete street name is legal, does the address fall within a valid address range, and if so, what is the CNN for the address?".

## Prerequisites

Python 3.x

PostgreSQL 10.x

* PostgreSQL table of San Francisco Addresses with Units from the Enterprise Addressing System (EAS)

    https://data.sfgov.org/Geographic-Locations-and-Boundaries/Addresses-with-Units-Enterprise-Addressing-System-/dxjs-vqsy

    https://data.sfgov.org/api/views/dxjs-vqsy/rows.csv?accessType=DOWNLOAD&bom=true&format=true 

* PostgreSQL table of San Francisco Basemap Street Centerlines

    https://data.sfgov.org/Geographic-Locations-and-Boundaries/San-Francisco-Basemap-Street-Centerlines/7hfy-8sz8

    https://data.sfgov.org/api/views/7hfy-8sz8/rows.csv?accessType=DOWNLOAD&bom=true&format=true

Command Line Interface (CLI) - The examples below use a Linux CLI.

## Routines

* **[csv2pg.py](csv2pg.py)** Imports a CSV file into a PostgreSQL table.

* **[remove_poboxes.py](remove_poboxes.py)** Alesco-specific routine that removes addresses that are P.O. Boxes. Specifically, it removes records with value of 'BOXHOLDER' in the 'slug' field of the Alesco input table.

* **[remove_duplicates.py](remove_duplicates.py)** Removes duplicate records of the 'address' field of the input table.

* **[parse_address_table.py](parse_address_table.py)** Parses, standarizes and validates addresses in a PostgreSQL table. It can save results to either a new table or append results to the existing addresses table.

* **[is_address_in_eas.py](is_address_in_eas.py)** Appends the input table with fields that answer the questions "Is this address in the EAS?", "Does the address have a legal San Francisco complete street name?", and "If the complete street name is legal, does the address fall within a valid address range, and if so, what is the CNN for the address?".

* **[pg2csv.py](pg2csv.py)** Exports a PostgreSQL table to a CSV file.

## Usage

Type `python script_name.py` in the command line with no arguments to see the complete list of arguments.

## Limit argument

Where applicable, the  `LIMIT` argument is passed to specify how many of the input records to process.

* *--LIMIT=0* will process no records. (Useful for confirming that arguments are valid without making any changes.)
* *--LIMIT=-1* will process all records.
* *--LIMIT=X* will process X records (where X is a positive integer).

## Output

* **[parse_address_table.py](parse_address_table.py)**

    ### All output fields are text fields unless otherwise noted

    ### Individual Address Parts

        address_number (integer)
        address_number_suffix
        street_name_pre_directional
        street_name_pre_type
        street_name
        street_name_post_type
        street_name_post_directional
        subaddress_type
        subaddress_identifier
        building_name
        place_name
        state_name
        zip_code
        zip_plus_4

    ### Combined Address Parts

    See also https://www.fgdc.gov/standards/projects/address-data/index_html

        complete_address_number = address_number + address_number_suffix

        complete_street_name = street_name_pre_directional + street_name_pre_type + street_name + street_name_post_type + street_name_post_directional

        complete_subaddress = building_name + subaddress_type + subaddress_identifier

        complete_place_name = place_name

    ### Complete Address

        delivery_address_without_unit = complete_address_number + complete_street_name

        delivery_address_with_unit = delivery_address_without_unit + complete_subaddress

    ###  Other Fields

        address_number_parity

    An Excel spreadsheet is also created that contains processing metadata and summaries of variables fields. The name if the spreadsheet is determined by a command-line argument.

* **[is_address_in_eas.py](is_address_in_eas.py)**

    The follow integer fields are appended to the input table:

        is_base_in_eas - Compare address, complete street name but NOT sub-unit
            -1 : Address not yet checked
            0 : Address is not in the EAS
            1 : Address is in the EAS

        is_sub_in_eas - Compare address, complete street name AND sub-unit, including blank values
            -1 : Address not yet checked
            0 : Address is not in the EAS
            1 : Address is in the EAS

        is_legal_complete_street_name (street_name + street_name_post_type)
            -1 : Complete street name not yet checked
            0 : Complete street name is not legal
            1 : Complete street name is legal 

        is_within_valid_range
            -1 : Address not yet checked or not checked because street name is not legal.
            0 : Address is not within valid address range.
            1 : Address is within valid address range.

        cnn
            The CNN value corresponding to the address, if the address has a legal complete street name and is within the valid range. Otherwise -1.

## TODO

* Sample usage
* Add tests
* Wrap in web services

## Authors

* **Dan King** https://bitbucket.org/dan-king
* Portions of logic and source code are from [imperfect_addresses](https://github.com/jonathanengelbert/imperfect_addresses) by **Jonathan Engelbert** https://github.com/jonathanengelbert

## License

See the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgments

* [The City and County Department of Technology, San Francisco](https://bitbucket.org/sfgovdt/)
* [San Francisco Enterprise Geographic Information Systems Program](https://sfgis-img.sfgov.org/)
* [Jonathan Engelbert](https://github.com/jonathanengelbert)
* [Federal Geographic Data Committee (FGDC) Geospatial Standards](https://www.fgdc.gov/standards)
