print('*******************************************************************************')
print('*******************************************************************************')
print('*******************************************************************************')
print('Begin script.')

# Import modules
import argparse # Parse CLI args
from datetime import datetime
import pandas as pd # Python Data Analysis Library # pip install pandas
import psycopg2 # PostgreSQL driver # pip install psycopg2
import sfgis_toolbox # Reusable routines by SFGIS
import sqlalchemy # SQL ORM  # pip install sqlalchemy
from sqlalchemy import create_engine
import sys

# Start script timer
start = datetime.now()
print(sys.argv[0] + 'start: {0}'.format(start))

# ######################################################################################
# Read command line arguments
# ######################################################################################
argparser=argparse.ArgumentParser()

# Define required database connection args
argparser.add_argument('--odbc_server', help='Database Server', type=str)
argparser.add_argument('--odbc_port',  help='Database Port', type=str)
argparser.add_argument('--odbc_database', help='Database Name', type=str)
argparser.add_argument('--odbc_uid', help='User Name', type=str)
argparser.add_argument('--odbc_pwd', help='User Password', type=str)

# Define I/O args
argparser.add_argument('--input_file', help='Input file (CSV)', type=str)
argparser.add_argument('--output_table', help='Output table', type=str)

# Optional args
argparser.add_argument('--sort_clause', help='Optional. Default is blank. Be sure to include the keywords "ORDER BY"', type=str, default='')

# Parse args
args=argparser.parse_args()

# Store args as local variables
odbc_server = args.odbc_server
odbc_port = args.odbc_port
odbc_database = args.odbc_database
odbc_uid = args.odbc_uid
odbc_pwd = args.odbc_pwd
input_file = args.input_file
output_table = args.output_table
sort_clause = args.sort_clause

# Print usage and halt if missing any required arguments
if any(elem is None for elem in [odbc_server, odbc_port, odbc_database, odbc_uid, odbc_pwd, input_file, output_table]):
    print(argparser.format_help())
    exit()

# ######################################################################################
# Define database connection info
# ######################################################################################
odbc_driver = '{psqlodbc}'

connection = None

# ######################################################################################
# Connect to database
# ######################################################################################

try:
    connection = psycopg2.connect(host=odbc_server, database=odbc_database, user=odbc_uid, password=odbc_pwd)
    cur = connection.cursor()

    # ######################################################################################
    # Copy the input csv to the output table
    # ######################################################################################
    # See https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.to_sql.html
    # See https://stackoverflow.com/questions/17662631/how-to-copy-from-csv-file-to-postgresql-table-with-headers-in-csv-file

    # TODO: Specify dtypes. See also:
    # https://stackoverflow.com/questions/24251219/pandas-read-csv-low-memory-and-dtype-options
    engine = create_engine('postgresql+psycopg2://'+odbc_uid+':'+odbc_pwd+'@'+odbc_server+':'+odbc_port+'/'+odbc_database)
    df = pd.read_csv(input_file)
    df.to_sql(output_table, engine, if_exists='replace', index=False)

#low_memory=False

    #------------------------------------------------------------------------------
    # Format field names (remove or replace spaces and special characters)
    #------------------------------------------------------------------------------
    sql = 'SELECT * FROM ' + output_table + ' LIMIT 1'
    cur.execute(sql)

    colnames = [desc[0] for desc in cur.description]
    print('Destination Columns: {0}'.format(colnames))
    print('-  -  -  -  -  -  -  -  -  -  -')
    for col in colnames:
        formatted_col = col.lower() # use lower case
        formatted_col = formatted_col.replace(' ', '_') # replace spaces with underscore
        formatted_col = formatted_col.replace('-', '_') # replace dashes with underscore
        formatted_col = formatted_col.replace('.', '_') # replace periods with underscore
        formatted_col = formatted_col.replace('"', '') # get rid of double quotes
        formatted_col = formatted_col.replace('#', '') # get rid of hashtags
        formatted_col = formatted_col.replace('?', '') # get rid of question marks

        # If fieldname leads with an integer then prepend with any character.
        first_char = formatted_col[0]
        # print("first_char: {0}".format(first_char))
        if sfgis_toolbox.validate_integer(first_char):
            formatted_col = 'a_'+ formatted_col

        # print('Column name: {0}'.format(col))
        # print('Formatted name: {0}'.format(formatted_col))
        if not(col == formatted_col):
            sql = 'ALTER TABLE ' + output_table + ' RENAME ' + '"'+col+'"' + ' TO ' + formatted_col
            print('sql: ' + sql)
            cur.execute(sql)
            connection.commit()

    #------------------------------------------------------------------------------
    # Sort the table
    #------------------------------------------------------------------------------
    sql = 'drop table if exists xxxtempsort'
    cur.execute(sql)

#    sql = 'create table xxxtempsort as select * from ' + output_table + ' ' + sort_clause
    sql = 'create table xxxtempsort as select * from ' + output_table
    print('sql:"{}"'.format(sql))
    cur.execute(sql)

    sql = 'drop table ' + output_table
    cur.execute(sql)

    sql = 'create table ' + output_table + ' as select * from xxxtempsort ' + sort_clause
    cur.execute(sql)

    sql = 'drop table xxxtempsort'
    cur.execute(sql)

    connection.commit()

    #------------------------------------------------------------------------------
    # Add SFGIS primary key
    #------------------------------------------------------------------------------
    # Let the program fail if the input table already has this column
    # Use this key for sorting for the remainder of the GAP steps. The final GAP step may drop this column.
    sql = 'ALTER TABLE ' + output_table + ' ADD COLUMN sfgisgapid SERIAL NOT NULL PRIMARY KEY;'
    print('sql: ' + sql)
    cur.execute(sql)
    connection.commit()

    # vs ALTER TABLE output_table ADD COLUMN idpk SERIAL PRIMARY KEY;

    #------------------------------------------------------------------------------
    # Dump the output table
    #------------------------------------------------------------------------------
    print('-  -  -  -  -  -  -  -  -  -  -')
    sql = 'SELECT * FROM ' + output_table
    cur.execute(sql)
    rows = cur.fetchall()
    print('Output Record Count: ' + str(len(rows)))

    colnames = [desc[0] for desc in cur.description]
    print('Output Columns: {0}'.format(colnames))

    limit_destination = ' LIMIT 1'
    sql = 'SELECT * FROM ' + output_table + ' ' + limit_destination
    print('Output SQL: ' + sql)
    cur.execute(sql)

    rows = cur.fetchall()
    for row in rows:
        print('Destination Row: {0}'.format(row))

finally:
    if connection:
        connection.close()

sfgis_toolbox.print_run_time(start)

print('*******************************************************************************')
print('                                                                               ')
print('End of script ' + sys.argv[0])
print('                                                                               ')
