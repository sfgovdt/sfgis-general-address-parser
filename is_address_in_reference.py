print('*******************************************************************************')
print('*******************************************************************************')
print('*******************************************************************************')
print('Begin script.')

# ######################################################################################
# Import modules
# ######################################################################################
import argparse # Parse CLI args
from datetime import datetime
import openpyxl # Library used to create report file
from openpyxl import Workbook
import psycopg2 # PostgreSQL driver
import re       #regular expressions
import sfgis_toolbox # Reusable routines by SFGIS
import sys

# ######################################################################################
# Start script timer
# ######################################################################################
start = datetime.now()
print(sys.argv[0] + ' start: {0}'.format(start))

# ######################################################################################
# Read command line arguments
# ######################################################################################
argparser=argparse.ArgumentParser()

# Required database connection args
argparser.add_argument('--odbc_server', help='Database Server', type=str)
argparser.add_argument('--odbc_port',  help='Database Port', type=str)
argparser.add_argument('--odbc_database', help='Database Name', type=str)
argparser.add_argument('--odbc_uid', help='User Name', type=str)
argparser.add_argument('--odbc_pwd', help='User Password', type=str)

# Source and reference table names
argparser.add_argument('--source_table', help='Source address table', type=str)
argparser.add_argument('--reference_table', help='Reference address table', type=str)

# Primary key names
argparser.add_argument('--source_pk', help='Source table primary key field', type=str)
argparser.add_argument('--reference_pk', help='Reference table primary key field', type=str)

# Source field names
argparser.add_argument('--source_address_number', help='Source table field source_address_number', type=str)
argparser.add_argument('--source_address_number_suffix', help='Source table field source_address_number_suffix', type=str)
argparser.add_argument('--source_street_name_pre_directional', help='Source table field source_street_name_pre_directional', type=str)
argparser.add_argument('--source_street_name_pre_type', help='Source table field source_street_name_pre_type', type=str)
argparser.add_argument('--source_street_name', help='Source table field source_street_name', type=str)
argparser.add_argument('--source_street_name_post_type', help='Source table field source_street_name_post_type', type=str)
argparser.add_argument('--source_street_name_post_directional', help='Source table field source_street_name_post_directional', type=str)
argparser.add_argument('--source_subaddress_type', help='Source table field source_subaddress_type', type=str)
argparser.add_argument('--source_subaddress_identifier', help='Source table field source_subaddress_identifier', type=str)
argparser.add_argument('--source_place_name', help='Source table field source_place_name', type=str)

# Reference field names
argparser.add_argument('--reference_address_number', help='Reference table field reference_address_number', type=str)
argparser.add_argument('--reference_address_number_suffix', help='Reference table field reference_address_number_suffix', type=str)
argparser.add_argument('--reference_street_name_pre_directional', help='Reference table field reference_street_name_pre_directional', type=str)
argparser.add_argument('--reference_street_name_pre_type', help='Reference table field reference_street_name_pre_type', type=str)
argparser.add_argument('--reference_street_name', help='Reference table field reference_street_name', type=str)
argparser.add_argument('--reference_street_name_post_type', help='Reference table field reference_street_name_post_type', type=str)
argparser.add_argument('--reference_street_name_post_directional', help='Reference table field reference_street_name_post_directional', type=str)
argparser.add_argument('--reference_subaddress_type', help='Reference table field reference_subaddress_type', type=str)
argparser.add_argument('--reference_subaddress_identifier', help='Reference table field reference_subaddress_identifier', type=str)
argparser.add_argument('--reference_place_name', help='Reference table field reference_place_name', type=str)

# Output field name
argparser.add_argument('--source_output_field', help='Output field name', type=str)

# Input record limit
argparser.add_argument('--limit', help='Limit records. Set to -1 for all records.', type=int, default=0)

# ######################################################################################
# Parse args
# ######################################################################################
args=argparser.parse_args()

# ######################################################################################
# Store args as local variables
# ######################################################################################
odbc_server = args.odbc_server
odbc_port = args.odbc_port
odbc_database = args.odbc_database
odbc_uid = args.odbc_uid
odbc_pwd = args.odbc_pwd

# Required source table args
source_table = args.source_table
source_pk = args.source_pk
source_output_field = args.source_output_field

# Required reference table args
reference_table = args.reference_table
reference_pk = args.reference_pk

# Optional source table args
source_address_number = args.source_address_number
source_address_number_suffix = args.source_address_number_suffix
source_street_name_pre_directional = args.source_street_name_pre_directional
source_street_name_pre_type = args.source_street_name_pre_type
source_street_name = args.source_street_name
source_street_name_post_type = args.source_street_name_post_type
source_street_name_post_directional = args.source_street_name_post_directional
source_subaddress_type = args.source_subaddress_type
source_subaddress_identifier = args.source_subaddress_identifier
source_place_name = args.source_place_name

# Optional reference table args
reference_address_number = args.reference_address_number
reference_address_number_suffix = args.reference_address_number_suffix
reference_street_name_pre_directional = args.reference_street_name_pre_directional
reference_street_name_pre_type = args.reference_street_name_pre_type
reference_street_name = args.reference_street_name
reference_street_name_post_type = args.reference_street_name_post_type
reference_street_name_post_directional = args.reference_street_name_post_directional
reference_subaddress_type = args.reference_subaddress_type
reference_subaddress_identifier = args.reference_subaddress_identifier
reference_place_name = args.reference_place_name

limit = args.limit

# ######################################################################################
# Print usage and halt if missing any required arguments
# ######################################################################################
if any(elem is None for elem in [odbc_server, odbc_port, odbc_database, odbc_uid, odbc_pwd, source_table, source_pk, reference_table, reference_pk, source_output_field]):
    print(argparser.format_help())
    exit()

# ######################################################################################
# Define database connection info
# ######################################################################################
odbc_driver = '{psqlodbc}'
connection = None

# ######################################################################################
# Define verbosity and limits of output
# ######################################################################################
verbose = 1
vverbose = 0

# Define query limits
if args.limit == -1:
    limit_source = ''
    limit_message = 'All records'
elif args.limit == 0:
    limit_source = ' LIMIT 0'
    limit_message = 'No records'
elif args.limit == 1:
    limit_source = ' LIMIT ' + format(limit)
    limit_message = '1 record'
elif args.limit > 1:
    limit_source = ' LIMIT ' + format(limit)
    limit_message = format(limit) + ' records'
else:
    print('Error. Invalid value for limit: {0}'.format(limit))
    exit()

# ######################################################################################
# Init record processing counter and other report variables
# ######################################################################################
counter = 0 # track input record count

print('Counter: ' + (str(counter)))
commitCounter = 0 # track iterations of records between commits
t1 = datetime.now() # track execution of the script

# Init report variables: sample data from first record
# (Init to blank in case the query returns 0 records)
source_record_count = 0

# ######################################################################################
# Connect to database
# ######################################################################################
try:
    connection = psycopg2.connect(host=odbc_server, database=odbc_database, user=odbc_uid, password=odbc_pwd)
    cur = connection.cursor()

    # ######################################################################################
    # Create output fields 
    # ######################################################################################
    create_output_field = True
    if create_output_field:
        print('BEGIN: Create output field')
        sql = 'ALTER TABLE ' + source_table + ' DROP COLUMN IF EXISTS ' + source_output_field
        print(sql)
        cur.execute(sql)
        connection.commit()
        sql = 'ALTER TABLE ' + source_table + ' ADD COLUMN ' + source_output_field + ' INTEGER NULL DEFAULT -1'
        print(sql)
        cur.execute(sql)
        connection.commit()
        print('END: Create output field')

    # ######################################################################################
    # Create index on the query fields
    # ######################################################################################
    create_index = True
    if create_index:
        print('BEGIN: Create index fields')
        # Index fields in source table
        sql = 'CREATE INDEX ON ' + source_table + ' ( ' + source_pk + ' )'
        cur.execute(sql)
        if source_address_number:
            sql = 'CREATE INDEX ON ' + source_table + ' ( ' + source_address_number + ' )'
            cur.execute(sql)
        if source_address_number_suffix:
            sql = 'CREATE INDEX ON ' + source_table + ' ( ' + source_address_number_suffix + ' )'
            cur.execute(sql)
        if source_street_name_pre_directional:
            sql = 'CREATE INDEX ON ' + source_table + ' ( ' + source_street_name_pre_directional + ' )'
            cur.execute(sql)
        if source_street_name_pre_type:
            sql = 'CREATE INDEX ON ' + source_table + ' ( ' + source_street_name_pre_type + ' )'
            cur.execute(sql)
        if source_street_name:
            sql = 'CREATE INDEX ON ' + source_table + ' ( ' + source_street_name + ' )'
            cur.execute(sql)
        if source_street_name_post_type:
            sql = 'CREATE INDEX ON ' + source_table + ' ( ' + source_street_name_post_type + ' )'
            cur.execute(sql)
        if source_street_name_post_directional:
            sql = 'CREATE INDEX ON ' + source_table + ' ( ' + source_street_name_post_directional + ' )'
            cur.execute(sql)
        if source_subaddress_type:
            sql = 'CREATE INDEX ON ' + source_table + ' ( ' + source_subaddress_type + ' )'
            cur.execute(sql)
        if source_subaddress_identifier:
            sql = 'CREATE INDEX ON ' + source_table + ' ( ' + source_subaddress_identifier + ' )'
            cur.execute(sql)
        if source_place_name:
            sql = 'CREATE INDEX ON ' + source_table + ' ( ' + source_place_name + ' )'
            cur.execute(sql)

        # Index fields in reference table
        sql = 'CREATE INDEX ON ' + reference_table + ' ( ' + reference_pk + ' )'
        cur.execute(sql)
        if reference_address_number:
            sql = 'CREATE INDEX ON ' + reference_table + ' ( ' + reference_address_number + ' )'
            cur.execute(sql)
        if reference_address_number_suffix:
            sql = 'CREATE INDEX ON ' + reference_table + ' ( ' + reference_address_number_suffix + ' )'
            cur.execute(sql)
        if reference_street_name_pre_directional:
            sql = 'CREATE INDEX ON ' + reference_table + ' ( ' + reference_street_name_pre_directional + ' )'
            cur.execute(sql)
        if reference_street_name_pre_type:
            sql = 'CREATE INDEX ON ' + reference_table + ' ( ' + reference_street_name_pre_type + ' )'
            cur.execute(sql)
        if reference_street_name:
            sql = 'CREATE INDEX ON ' + reference_table + ' ( ' + reference_street_name + ' )'
            cur.execute(sql)
        if reference_street_name_post_type:
            sql = 'CREATE INDEX ON ' + reference_table + ' ( ' + reference_street_name_post_type + ' )'
            cur.execute(sql)
        if reference_street_name_post_directional:
            sql = 'CREATE INDEX ON ' + reference_table + ' ( ' + reference_street_name_post_directional + ' )'
            cur.execute(sql)
        if reference_subaddress_type:
            sql = 'CREATE INDEX ON ' + reference_table + ' ( ' + reference_subaddress_type + ' )'
            cur.execute(sql)
        if reference_subaddress_identifier:
            sql = 'CREATE INDEX ON ' + reference_table + ' ( ' + reference_subaddress_identifier + ' )'
            cur.execute(sql)
        if reference_place_name:
            sql = 'CREATE INDEX ON ' + reference_table + ' ( ' + reference_place_name + ' )'
            cur.execute(sql)

        connection.commit()
        print('END: Create index fields')

    # ######################################################################################
    # Check that all reference fields exist
    # ######################################################################################
    sql = 'SELECT * FROM ' + reference_table + ' LIMIT 1'
    cur.execute(sql)
    reference_column_list = [desc[0] for desc in cur.description]
    if verbose:
        print('Reference Columns: {0}'.format(reference_column_list))
    if not reference_pk in reference_column_list:
        print('***** Error. Did not reference field "{}" in reference table.'.format(reference_pk))
        print('Fields in reference table "{}": {}'.format(reference_table, reference_column_list))
        exit()
    if reference_address_number:
        if not reference_address_number in reference_column_list:
            print('***** Error. Did not reference field "{}" in reference table.'.format(reference_address_number))
            print('Fields in reference table "{}": {}'.format(reference_table, reference_column_list))
            exit()
    if reference_address_number_suffix:
        if not reference_address_number_suffix in reference_column_list:
            print('***** Error. Did not reference field "{}" in reference table.'.format(reference_address_number_suffix))
            print('Fields in reference table "{}": {}'.format(reference_table, reference_column_list))
            exit()
    if reference_street_name_pre_directional:
        if not reference_street_name_pre_directional in reference_column_list:
            print('***** Error. Did not reference field "{}" in reference table.'.format(reference_street_name_pre_directional))
            print('Fields in reference table "{}": {}'.format(reference_table, reference_column_list))
            exit()
    if reference_street_name_pre_type:
        if not reference_street_name_pre_type in reference_column_list:
            print('***** Error. Did not reference field "{}" in reference table.'.format(reference_street_name_pre_type))
            print('Fields in reference table "{}": {}'.format(reference_table, reference_column_list))
            exit()
    if reference_street_name:
        if not reference_street_name in reference_column_list:
            print('***** Error. Did not reference field "{}" in reference table.'.format(reference_street_name))
            print('Fields in reference table "{}": {}'.format(reference_table, reference_column_list))
            exit()
    if reference_street_name_post_type:
        if not reference_street_name_post_type in reference_column_list:
            print('***** Error. Did not reference field "{}" in reference table.'.format(reference_street_name_post_type))
            print('Fields in reference table "{}": {}'.format(reference_table, reference_column_list))
            exit()
    if reference_street_name_post_directional:
        if not reference_street_name_post_directional in reference_column_list:
            print('***** Error. Did not reference field "{}" in reference table.'.format(reference_street_name_post_directional))
            print('Fields in reference table "{}": {}'.format(reference_table, reference_column_list))
            exit()
    if reference_subaddress_type:
        if not reference_subaddress_type in reference_column_list:
            print('***** Error. Did not reference field "{}" in reference table.'.format(reference_subaddress_type))
            print('Fields in reference table "{}": {}'.format(reference_table, reference_column_list))
            exit()
    if reference_subaddress_identifier:
        if not reference_subaddress_identifier in reference_column_list:
            print('***** Error. Did not reference field "{}" in reference table.'.format(reference_subaddress_identifier))
            print('Fields in reference table "{}": {}'.format(reference_table, reference_column_list))
            exit()
    if reference_place_name:
        if not reference_place_name in reference_column_list:
            print('***** Error. Did not reference field "{}" in reference table.'.format(reference_place_name))
            print('Fields in reference table "{}": {}'.format(reference_table, reference_column_list))
            exit()

    # ######################################################################################
    # Get source record count
    # ######################################################################################
    sql = 'SELECT COUNT(*) AS count FROM ' + source_table
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    source_record_count = row[0]
    print('source_record_count: {0}'.format(source_record_count))

    # ######################################################################################
    # Get source records
    # ######################################################################################
    sql = 'SELECT * FROM ' + source_table + ' ORDER BY ' + source_pk
    sql += limit_source
    cur.execute(sql)

    # Echo source column names
    source_column_list = [desc[0] for desc in cur.description]
    if verbose:
        print('Source Columns: {0}'.format(source_column_list))

    # Find the column index for each source field. Halt if field not found.
    if source_pk in source_column_list:
        source_pk_index = source_column_list.index(source_pk)
        if verbose:
            print('source_pk_index:{}'.format(source_pk_index))
    else:
        print('***** Error. Did not source field "{}" in source table.'.format(source_pk))
        print('Fields in source table "{}": {}'.format(source_table, source_column_list))
        exit()
    if source_address_number:
        if source_address_number in source_column_list:
            source_address_number_index = source_column_list.index(source_address_number)
            if verbose:
                print('source_address_number_index:{}'.format(source_address_number_index))
        else:
            print('***** Error. Did not source field "{}" in source table.'.format(source_address_number))
            print('Fields in source table "{}": {}'.format(source_table, source_column_list))
            exit()
    if source_address_number_suffix:
        if source_address_number_suffix in source_column_list:
            source_address_number_suffix_index = source_column_list.index(source_address_number_suffix)
            if verbose:
                print('source_address_number_suffix_index:{}'.format(source_address_number_suffix_index))
        else:
            print('***** Error. Did not source field "{}" in source table.'.format(source_address_number_suffix))
            print('Fields in source table "{}": {}'.format(source_table, source_column_list))
            exit()
    if source_street_name_pre_directional:
        if source_street_name_pre_directional in source_column_list:
            source_street_name_pre_directional_index = source_column_list.index(source_street_name_pre_directional)
            if verbose:
                print('source_street_name_pre_directional_index:{}'.format(source_street_name_pre_directional_index))
        else:
            print('***** Error. Did not source field "{}" in source table.'.format(source_street_name_pre_directional))
            print('Fields in source table "{}": {}'.format(source_table, source_column_list))
            exit()
    if source_street_name_pre_type:
        if source_street_name_pre_type in source_column_list:
            source_street_name_pre_type_index = source_column_list.index(source_street_name_pre_type)
            if verbose:
                print('source_street_name_pre_type_index:{}'.format(source_street_name_pre_type_index))
        else:
            print('***** Error. Did not source field "{}" in source table.'.format(source_street_name_pre_type))
            print('Fields in source table "{}": {}'.format(source_table, source_column_list))
            exit()
    if source_street_name:
        if source_street_name in source_column_list:
            source_street_name_index = source_column_list.index(source_street_name)
            if verbose:
                print('source_street_name_index:{}'.format(source_street_name_index))
        else:
            print('***** Error. Did not source field "{}" in source table.'.format(source_street_name))
            print('Fields in source table "{}": {}'.format(source_table, source_column_list))
            exit()
    if source_street_name_post_type:
        if source_street_name_post_type in source_column_list:
            source_street_name_post_type_index = source_column_list.index(source_street_name_post_type)
            if verbose:
                print('source_street_name_post_type_index:{}'.format(source_street_name_post_type_index))
        else:
            print('***** Error. Did not source field "{}" in source table.'.format(source_street_name_post_type))
            print('Fields in source table "{}": {}'.format(source_table, source_column_list))
            exit()
    if source_street_name_post_directional:
        if source_street_name_post_directional in source_column_list:
            source_street_name_post_directional_index = source_column_list.index(source_street_name_post_directional)
            if verbose:
                print('source_street_name_post_directional_index:{}'.format(source_street_name_post_directional_index))
        else:
            print('***** Error. Did not source field "{}" in source table.'.format(source_street_name_post_directional))
            print('Fields in source table "{}": {}'.format(source_table, source_column_list))
            exit()
    if source_subaddress_type:
        if source_subaddress_type in source_column_list:
            source_subaddress_type_index = source_column_list.index(source_subaddress_type)
            if verbose:
                print('source_subaddress_type_index:{}'.format(source_subaddress_type_index))
        else:
            print('***** Error. Did not source field "{}" in source table.'.format(source_subaddress_type))
            print('Fields in source table "{}": {}'.format(source_table, source_column_list))
            exit()
    if source_subaddress_identifier:
        if source_subaddress_identifier in source_column_list:
            source_subaddress_identifier_index = source_column_list.index(source_subaddress_identifier)
            if verbose:
                print('source_subaddress_identifier_index:{}'.format(source_subaddress_identifier_index))
        else:
            print('***** Error. Did not source field "{}" in source table.'.format(source_subaddress_identifier))
            print('Fields in source table "{}": {}'.format(source_table, source_column_list))
            exit()
    if source_place_name:
        if source_place_name in source_column_list:
            source_place_name_index = source_column_list.index(source_place_name)
            if verbose:
                print('source_place_name_index:{}'.format(source_place_name_index))
        else:
            print('***** Error. Did not source field "{}" in source table.'.format(source_place_name))
            print('Fields in source table "{}": {}'.format(source_table, source_column_list))
            exit()

    rows = cur.fetchall()
    for row in rows:
        counter+=1
        if counter == 1:
            first_source_row = row
            if verbose:
                print('=================================')
                print('First Source Row: {}'.format(first_source_row))
        if vverbose:
            print('---------------------------------')
            print('{}. Source Row: {}'.format(counter, row))

        # ######################################################################################
        # Answer Question: Is the source address in the reference table? 
        # If so then set flag true, otherwise set flag false.
        # ######################################################################################
        sql = 'SELECT ' + reference_pk
        sql += ' FROM ' + reference_table
        sql += ' WHERE 1=1 '
        if source_address_number:
            source_address_number_value = row[source_address_number_index]
            if vverbose:
                print('source_address_number_value: {}'.format(source_address_number_value))
            sql += ' AND ' +  reference_address_number + ' = ' + str(source_address_number_value) # address_number is the only address field that is an integer. Cast it to a string for the purposes of creating the SQL variable string.
        if source_address_number_suffix:
            source_address_number_suffix_value = row[source_address_number_suffix_index]
            if vverbose:
                print('source_address_number_suffix_value: {}'.format(source_address_number_suffix_value))
            sql += ' AND ' + reference_address_number_suffix + ' = ' + "'" + source_address_number_suffix_value + "'"
        if source_street_name_pre_directional:
            source_street_name_pre_directional_value = row[source_street_name_pre_directional_index]
            if vverbose:
                print('source_street_name_pre_directional_value: {}'.format(source_street_name_pre_directional_value))
            sql += ' AND ' + reference_street_name_pre_directional + ' = ' + "'" + source_street_name_pre_directional_value + "'"
        if source_street_name_pre_type:
            source_street_name_pre_type_value = row[source_street_name_pre_type_index]
            if vverbose:
                print('source_street_name_pre_type_value: {}'.format(source_street_name_pre_type_value))
            sql += ' AND ' + reference_street_name_pre_type + ' = ' + "'" + source_street_name_pre_type_value + "'"
        if source_street_name:
            source_street_name_value = row[source_street_name_index]
            if vverbose:
                print('source_street_name_value: {}'.format(source_street_name_value))
            sql += ' AND ' + reference_street_name + ' = ' + "'" + source_street_name_value + "'"
        if source_street_name_post_type:
            source_street_name_post_type_value = row[source_street_name_post_type_index]
            if vverbose:
                print('source_street_name_post_type_value: {}'.format(source_street_name_post_type_value))
            sql += ' AND ' + reference_street_name_post_type + ' = ' + "'" + source_street_name_post_type_value + "'"
        if source_street_name_post_directional:
            source_street_name_post_directional_value = row[source_street_name_post_directional_index]
            if vverbose:
                print('source_street_name_post_directional_value: {}'.format(source_street_name_post_directional_value))
            sql += ' AND ' + reference_street_name_post_directional + ' = ' + "'" + source_street_name_post_directional_value + "'"
        if source_subaddress_type:
            source_subaddress_type_value = row[source_subaddress_type_index]
            if vverbose:
                print('source_subaddress_type_value: {}'.format(source_subaddress_type_value))
            sql += ' AND ' + reference_subaddress_type + ' = ' + "'" + source_subaddress_type_value + "'"
        if source_subaddress_identifier:
            source_subaddress_identifier_value = row[source_subaddress_identifier_index]
            if vverbose:
                print('source_subaddress_identifier_value: {}'.format(source_subaddress_identifier_value))
            sql += ' AND ' + reference_subaddress_identifier + ' = ' + "'" + source_subaddress_identifier_value + "'"
        if source_place_name:
            source_place_name_value = row[source_place_name_index]
            if vverbose:
                print('source_place_name_value: {}'.format(source_place_name_value))
            sql += ' AND ' + reference_place_name + ' = ' + "'" + source_place_name_value + "'"
        sql += ' LIMIT 1 '

        if vverbose:
            print('sql: {}'.format(sql))
 
        cur.execute(sql)
        reference_rows = cur.fetchall()
        is_source_in_reference = 0
        if len(reference_rows):
            is_source_in_reference = 1

        if vverbose:
            print('is_source_in_reference:{}'.format(is_source_in_reference))

        # ######################################################################################
        #  Update the record in the input table
        # ######################################################################################
        source_pk_value = row[source_pk_index]
        if vverbose:
            print('source_pk_value: {}'.format(source_pk_value))
        sql = 'UPDATE ' + source_table
        sql += ' SET ' + source_output_field
        sql += ' = %s'
        sql += ' WHERE ' + source_pk + ' = ' + format(source_pk_value)
        if vverbose:
            print('sql: {}'.format(sql))
        cur.execute(sql, [is_source_in_reference])

        # ######################################################################################
        # Commit the changes every so often
        # ######################################################################################
        commitCounter += 1
        commitFrequency = 10000
        if (commitCounter % commitFrequency == 0):
            connection.commit()

            t2 = datetime.now()
            tdelta = t2 - t1

            if verbose:
                print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
                print('                                                                               ')
                print('** Script processing time **')
                print('                                                                               ')
                print('Counter at ' + str(counter) + ' of ' + str(source_record_count))
                print('Committing at ' + str(commitCounter))
                print('Delta: {0} per {1}'.format(tdelta, commitFrequency))

                current = datetime.now()
                elapsed = current - start
                print('Script start time: {0}'.format(start))
                print('Script current time:   {0}'.format(current))
                print('Script run time:               {0}'.format(elapsed))

                # seconds per records
                elapsed_secs = elapsed.seconds
                recs_per_secs = counter / elapsed_secs
                print('recs_per_secs: {:,.0f}'.format(recs_per_secs))

                # Total seconds est
                est_total_secs = source_record_count / recs_per_secs
                print('est_total_secs: {:,.0f}'.format(est_total_secs))

                # Seconds remaining
                est_remain_secs = est_total_secs - elapsed_secs
                print('est_remain_secs: {:,.0f}'.format(est_remain_secs))
                if (est_remain_secs > 60):
                    est_remain_mins = est_remain_secs / 60
                    print('est_remain_mins: {:,.0f}'.format(est_remain_mins))

            t1 = t2 #reset t1 for the next iteration
            commitCounter = 0

    # Final commit
    connection.commit()

finally:
    if verbose:
        print('Done processing records.')

# ######################################################################################
# Calculate run time 
# ######################################################################################
# Calculate processing time, time per recs ,  recs per time
end = datetime.now()
elapased = end - start
# Convert elapsed time into seconds
elapased_seconds = sfgis_toolbox.get_sec(format(elapased))
# Time per 1 record
if counter:
    seconds_per_record = elapased_seconds / counter
else:
    seconds_per_record = 0
# Rec/sec
records_per_second = counter / elapased_seconds

# Close database connection
if connection:
    connection.close()

# ######################################################################################
# Display results
# ######################################################################################
print('                                                                               ')
print('===============================================================================')
print('                                                                               ')
print(sys.argv[0])
print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('Source Table: {0}'.format(source_table))
print('Source Count: {0}'.format(source_record_count))
print('Source Record Limit: ' + limit_message)
print('Source Records Processed: ' + (str(counter)))
print('                                                                               ')
print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
print('                                                                               ')
print('Reference Table: {0}'.format(reference_table))
print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('** Processing time estimates **')
print('                                                                               ')
print('Seconds to process 1 record: {:,.4f}'.format(seconds_per_record))
print('Seconds to process 1000 records: {:,.2f}'.format(seconds_per_record * 1000))
print('Minutes to process 1,000,000 records: {:,.1f}'.format(seconds_per_record * 1000000 / 60))
print('                                                                               ')
print('Records processed per second: {:,.0f}'.format(records_per_second))
print('Records processed per minute: {:,.0f}'.format(records_per_second * 60))
print('Records processed per hour: {:,.0f}'.format(records_per_second * 60 * 60))
print('                                                                               ')
print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
print('                                                                               ')
print('** Record processing time **')
print('                                                                               ')
print('Seconds to process {0} records: {1:,.4f}'.format(counter, elapased_seconds))
print('                                                                               ')
sfgis_toolbox.print_run_time(start)
print('*******************************************************************************')
print('                                                                               ')
print('End of script ' + sys.argv[0])
print('                                                                               ')
