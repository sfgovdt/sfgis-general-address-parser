print('*******************************************************************************')
print('*******************************************************************************')
print('*******************************************************************************')
print('Begin script.')

# Import modules
import argparse # Parse CLI args
import csv
from datetime import datetime
import openpyxl # Library used to create report file
from openpyxl import Workbook
import pandas as pd
import psycopg2 # PostgreSQL driver
import sfgis_toolbox # Reusable routines by SFGIS
import sqlalchemy # SQL ORM  # pip install sqlalchemy
from sqlalchemy import create_engine
import sys

# Start script timer
start = datetime.now()
print(sys.argv[0] + 'start: {0}'.format(start))

# ######################################################################################
# Read command line arguments
# ######################################################################################
argparser=argparse.ArgumentParser()

# Define required database connection args
argparser.add_argument('--odbc_server', help='Database Server', type=str)
argparser.add_argument('--odbc_port',  help='Database Port', type=str)
argparser.add_argument('--odbc_database', help='Database Name', type=str)
argparser.add_argument('--odbc_uid', help='User Name', type=str)
argparser.add_argument('--odbc_pwd', help='User Password', type=str)

# Define I/O args
argparser.add_argument('--input_table', help='Input table', type=str)
argparser.add_argument('--output_file_name_prefix', help='Optional output file name. Default is "bulkloader.csv"', default='bulkloader.csv', type=str)
argparser.add_argument('--input_source', help='Description for the Bulk Loader "source" field (32 character max)', type=str)
argparser.add_argument('--address_field', help='Optional. Name of address field.', type=str, default='delivery_address_with_unit')
argparser.add_argument('--block_field', help='Optional. Name of block field.', type=str, default='block')
argparser.add_argument('--lot_field', help='Optional. Name of lot field.', type=str, default='lot')
argparser.add_argument('--unit_field', help='Optional. Name of unit field.', type=str, default='subaddress_identifier')
argparser.add_argument('--street_number_field', help='Optional. Name of street number field.', type=str, default='address_number')
argparser.add_argument('--street_name_field', help='Optional. Name of street name field.', type=str, default='street_name')
argparser.add_argument('--street_sfx_field', help='Optional. Name of street_sfx (direction) field.', type=str, default='street_name_post_directional')
argparser.add_argument('--street_type_field', help='Optional. Name of street type field.', type=str, default='street_name_post_type')
argparser.add_argument('--zip_code_field', help='Optional. Name of zip code field.', type=str, default='zip_code')
argparser.add_argument('--where_clause', help='Optional WHERE clause to filter input', default='', type=str)
argparser.add_argument('--limit', help='Limit records. Set to -1 for all records.', type=int, default=0)

# Parse args
args=argparser.parse_args()

# Store args as local variables
odbc_server = args.odbc_server
odbc_port = args.odbc_port
odbc_database = args.odbc_database
odbc_uid = args.odbc_uid
odbc_pwd = args.odbc_pwd
input_table = args.input_table
output_file_name_prefix = args.output_file_name_prefix
input_source = args.input_source
address_field = args.address_field
block_field = args.block_field
lot_field = args.lot_field
unit_field = args.unit_field
street_number_field = args.street_number_field
street_name_field = args.street_name_field
street_sfx_field = args.street_sfx_field
street_type_field = args.street_type_field
where_clause = args.where_clause
zip_code_field = args.zip_code_field
limit = args.limit

# Print usage and halt if missing any required arguments
if any(elem is None for elem in [odbc_server, odbc_port, odbc_database, odbc_uid, odbc_pwd, input_table, output_file_name_prefix]):
    print(argparser.format_help())
    exit()

# ######################################################################################
# Define database connection info
# ######################################################################################
odbc_driver = '{psqlodbc}'

connection = None

# ######################################################################################
# Define query limits
# ######################################################################################
limit_source = ' LIMIT ' + format(limit)
if limit_source == '':
    limit_message = 'All records'
elif args.limit == 0:
    limit_source = ' LIMIT 0'
    limit_message = 'No records'
elif args.limit == 1:
    #limit_source = ' LIMIT ' + format(limit+1) # Add 1 because the postgresql limit is a ceiling
    limit_message = '1 record'
elif args.limit > 1:
    #limit_source = ' LIMIT ' + format(limit+1) # Add 1 because the postgresql limit is a ceiling
    limit_message = format(limit) + ' records'
else:
    print('Error. Invalid value for limit: {0}'.format(limit))
    exit()

# ######################################################################################
# Connect to database
# ######################################################################################

try:
    connection = psycopg2.connect(host=odbc_server, database=odbc_database, user=odbc_uid, password=odbc_pwd)
    cur = connection.cursor()


    print('made a connection')
    exit()
    # ######################################################################################
    # Create temporary table containing the complete exported set.
    # ######################################################################################
    temp_table = 'gap_temp_table'
    sql = 'DROP TABLE IF EXISTS ' + temp_table
    cur.execute(sql)
    connection.commit()

    sql = 'CREATE TABLE ' + temp_table + ' AS '
    sql += ' SELECT '
    sql += ' sfgisgapid, '

    sql += address_field + ' AS address '
    # Do not attempt to specify block or lot for new records.
    sql += ', ' + "'' AS block "
    sql += ', ' + "'' AS lot "
    #sql += ', ' + block_field + ' AS block '
    #sql += ', ' + lot_field + ' AS lot '
    sql += ', ' + unit_field + ' AS unit '
    sql += ", '' AS unit_sfx "

    # CSV-specific field names.
    # sql += ', ' + street_number_field + ' AS street_number '
    # sql += ", '' AS street_number_sfx "
    # sql += ', ' + street_name_field + ' AS street_name '
    # sql += ', ' + street_sfx_field + ' AS street_sfx '
    # sql += ', ' + street_type_field + ' AS street_type '
    # sql += ", '" + input_source + "' AS load_source "

    # SHP-specific field names.
    sql += ', ' + street_number_field + ' AS st_num '
    sql += ", '' AS st_num_sfx "
    sql += ', ' + street_name_field + ' AS st_name '
    sql += ', ' + street_sfx_field + ' AS st_suffix '
    sql += ', ' + street_type_field + ' AS st_type '
    sql += ", '" + input_source + "' AS source "

    sql += ', ' + zip_code_field + ' AS zip '
    sql += ' FROM ' + input_table
    sql += ' WHERE 1=1 '
    #sql += ' AND is_base_in_eas = 0 '
    #sql += ' AND is_for_bulk_loader = 1 '
    sql += ' ' + where_clause
    sql += ' ORDER BY sfgisgapid'
    sql += limit_source
    print('-  -  -  -  -  -  -  -  -  -  -')
    print('sql: {}'.format(sql))
    cur.execute(sql)
    connection.commit()

    # ######################################################################################
    # Export the input table to the output CSV(s)
    # ######################################################################################
    engine = create_engine('postgresql+psycopg2://'+odbc_uid+':'+odbc_pwd+'@'+odbc_server+':'+odbc_port+'/'+odbc_database)

    # ------------------------------------------------------------------------------
    # Export 1: All records
    # ------------------------------------------------------------------------------
    sql = ' SELECT * FROM ' + temp_table
    sql += ' WHERE 1=1 '
    sql += ' ORDER BY sfgisgapid'
    print('-  -  -  -  -  -  -  -  -  -  -')
    print('sql: {}'.format(sql))
    df = pd.read_sql_query(sql,con=engine)
    print('rows:{}'.format(len(df.index)))
    output_file_name_1 = output_file_name_prefix + '_all.csv'
    df.to_csv(output_file_name_1, quoting=csv.QUOTE_NONNUMERIC, index=False)

    # ------------------------------------------------------------------------------
    # Export 2: Just base records
    # ------------------------------------------------------------------------------
    sql = ' SELECT * FROM ' + temp_table
    sql += ' WHERE 1=1 '
    sql += " AND unit LIKE '' "
    sql += ' ORDER BY sfgisgapid'
    print('-  -  -  -  -  -  -  -  -  -  -')
    print('sql: {}'.format(sql))
    df = pd.read_sql_query(sql,con=engine)
    print('rows:{}'.format(len(df.index)))
    output_file_name_2 = output_file_name_prefix + '_base.csv'
    df.to_csv(output_file_name_2, quoting=csv.QUOTE_NONNUMERIC, index=False)

    # ------------------------------------------------------------------------------
    # Export 3: Just unit records
    # ------------------------------------------------------------------------------
    sql = ' SELECT * FROM ' + temp_table
    sql += ' WHERE 1=1 '
    sql += " AND NOT unit LIKE '' "
    sql += ' ORDER BY sfgisgapid'
    print('-  -  -  -  -  -  -  -  -  -  -')
    print('sql: {}'.format(sql))
    df = pd.read_sql_query(sql,con=engine)
    print('rows:{}'.format(len(df.index)))
    output_file_name_3 = output_file_name_prefix + '_unit.csv'
    df.to_csv(output_file_name_3, quoting=csv.QUOTE_NONNUMERIC, index=False)

    # ######################################################################################
    # Drop temporary table
    # ######################################################################################
    sql = 'DROP TABLE IF EXISTS ' + temp_table
    cur.execute(sql)
    connection.commit()

    # ------------------------------------------------------------------------------
    # Dump the input table
    # ------------------------------------------------------------------------------
    print('-  -  -  -  -  -  -  -  -  -  -')
    sql = 'SELECT count(sfgisgapid) AS Count'
    sql += ' FROM ' + input_table
    cur.execute(sql)
    input_row_all = cur.fetchall()
    if len(input_row_all):
        input_row_count = input_row_all[0][0] # First record, first column will have count
    else:
        input_row_count = -1
    print('Input Record Count: ' + str(input_row_count))
    print('-  -  -  -  -  -  -  -  -  -  -')

    sql = 'SELECT * FROM ' + input_table
    sql += ' WHERE 1=1 '
    sql += ' ' + where_clause
    sql += ' ORDER BY sfgisgapid'
    sql += ' LIMIT 1'
    print('Input SQL: ' + sql)
    print('-  -  -  -  -  -  -  -  -  -  -')
    cur.execute(sql)
    colnames = [desc[0] for desc in cur.description]
    print('Input Columns: {0}'.format(colnames))
    print('-  -  -  -  -  -  -  -  -  -  -')


    rows = cur.fetchall()
    cur.execute(sql)

    rows = cur.fetchall()
    for row in rows:
        print('Row: {0}'.format(row))
        print('-  -  -  -  -  -  -  -  -  -  -')

finally:
    if connection:
        connection.close()

sfgis_toolbox.print_run_time(start)

print('*******************************************************************************')
print('                                                                               ')
print('End of script ' + sys.argv[0])
print('                                                                               ')
