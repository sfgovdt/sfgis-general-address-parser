print('*******************************************************************************')
print('*******************************************************************************')
print('*******************************************************************************')
print('Begin script.')

# ######################################################################################
# Import modules
# ######################################################################################
import argparse # Parse CLI args
from datetime import datetime
import numbers
import openpyxl # Library used to create report file
from openpyxl import Workbook
import psycopg2 # PostgreSQL driver
import re       #regular expressions
import sfgis_toolbox # Reusable routines by SFGIS
import sfcpc_toolbox # Reusable routines by SF Planning (CPC)
import sys
import usaddress # Parserator 'usaddress' parsing module # See https://parserator.datamade.us/api-docs

# ######################################################################################
# Start script timer
# ######################################################################################
start = datetime.now()
print(sys.argv[0] + ' start: {0}'.format(start))

# ######################################################################################
# Read command line arguments
# ######################################################################################
argparser=argparse.ArgumentParser()

# Required database connection args
argparser.add_argument('--odbc_server', help='Database Server', type=str)
argparser.add_argument('--odbc_port',  help='Database Port', type=str)
argparser.add_argument('--odbc_database', help='Database Name', type=str)
argparser.add_argument('--odbc_uid', help='User Name', type=str)
argparser.add_argument('--odbc_pwd', help='User Password', type=str)

# Required args
argparser.add_argument('--address_table', help='Input address table', type=str)
argparser.add_argument('--primary_key', help='Primary key of th input table', type=str)
argparser.add_argument('--output_table', help='Output address table', type=str)
argparser.add_argument('--output_report_file', help='Output report file (CSV or XLSX)', type=str)

# Optional args
argparser.add_argument('--street_number_column', help='Optional. Street Number field', type=str, default='')
argparser.add_argument('--street_number_suffix_column', help='Optional. Street Number Suffix field', type=str, default='')
argparser.add_argument('--address_column', help='Optional. Column containing complete address or just the street name.', type=str, default='')
argparser.add_argument('--street_name_suffix_column', help='Optional. Column containing address name suffix', type=str, default='')
argparser.add_argument('--subaddress_type_column', help='Optional. Column containing address unit', type=str, default='')
argparser.add_argument('--subaddress_id_column', help='Optional. Column containing address unit suffix', type=str, default='')
argparser.add_argument('--city_column', help='Optional. Column containing city name', type=str, default='')
argparser.add_argument('--state_column', help='Optional. Column containing state name', type=str, default='')
argparser.add_argument('--zip_column', help='Optional. Column containing ZIP code', type=str, default='')
argparser.add_argument('--all_sf', help='Optional. Set to "yes" to set all city and state values to "SAN FRANCISCO, CA" regardless of parsed results.', type=str, default='no')
argparser.add_argument('--address_type_column', help='Optional. Address prefix field', type=str, default='')
argparser.add_argument('--alesco_address_type_column', help='Optional. Alesco "Address Type" field', type=str, default='')
argparser.add_argument('--limit', help='Limit records. Set to -1 for all records.', type=int, default=0)

# TODO: Add optional arguments predirectional_column,postdirectional_column
# TODO: Rename argument zip_column to postal_code_column

# Parse args
args=argparser.parse_args()

# Store args as local variables
odbc_server = args.odbc_server
odbc_port = args.odbc_port
odbc_database = args.odbc_database
odbc_uid = args.odbc_uid
odbc_pwd = args.odbc_pwd
address_table = args.address_table
primary_key = args.primary_key
output_table = args.output_table
output_report_file = args.output_report_file
street_number_column = args.street_number_column
street_number_suffix_column = args.street_number_suffix_column
address_column = args.address_column
street_name_suffix_column = args.street_name_suffix_column
subaddress_type_column = args.subaddress_type_column
subaddress_id_column = args.subaddress_id_column
city_column = args.city_column
state_column = args.state_column
zip_column = args.zip_column
all_sf = args.all_sf
address_type_column = args.address_type_column
alesco_address_type_column = args.alesco_address_type_column
limit = args.limit

# Print usage and halt if missing any required arguments
if any(elem is None for elem in [odbc_server, odbc_port, odbc_database, odbc_uid, odbc_pwd, address_table, primary_key, output_table, output_report_file]):
    print(argparser.format_help())
    exit()

# ######################################################################################
# Define database connection info
# ######################################################################################
odbc_driver = '{psqlodbc}'

connection = None

# ######################################################################################
# Define verbosity and limits of output
# ######################################################################################
verbose = 1
vverbose = 0

# Define query limits
if args.limit == -1:
    limit_source = ''
    limit_message = 'All records'
elif args.limit == 0:
    limit_source = ' LIMIT 0'
    limit_message = 'No records'
elif args.limit == 1:
    limit_source = ' LIMIT 1'
    limit_message = '1 record'
elif args.limit > 1:
    limit_source = ' LIMIT ' + format(limit)
    limit_message = format(limit) + ' records'
else:
    print('Error. Invalid value for limit: {0}'.format(limit))
    exit()

# ######################################################################################
# Validate input. Init record processing counter and other report variables
# ######################################################################################

# Fail if input and output table are the same
if address_table == output_table:
    print('Error. Input values for address_table and output_table are the same: "{}","{}". They should be different.'.format(address_table, output_table))
    exit()


counter = 0 # track input record count
parser_issue_count = 0 # track number of records with parsing issues
custom_parse_count = 0 # track number of records custom parsed

print('Counter: ' + (str(counter)))
commitCounter = 0 # track iterations of records between commits
t1 = datetime.now() # track execution of the script

# Init report variables: sample data from first record
# (Init to blank in case the query returns 0 records)
input_record_count = 0
input_column_list = []
first_input_row = []
first_address_parsed = '' # Init the first address we send to be parsed
first_parsed_address = '' # Init the parse results of the first address
#found_street_name_count = 0 # Number of records where street name found in EAS
#not_found_street_name_count = 0
record_skip_count = 0
modified_by_cpc = False
modified_by_cpc_count = 0

output_record_count = 0
output_column_list = []
first_output_row = []

# ######################################################################################
# Define constants
# ######################################################################################
DEFAULT_CITY = 'SAN FRANCISCO'
DEFAULT_STATE = 'CA'
DEFAULT_COUNTRY = 'USA'

# ######################################################################################
# Function to parse an address
# ######################################################################################
# TODO: Replace 'global's with object/dictionary passed back and forth
def parse_address(row):
    global parser_had_issues
    global custom_parsed
    global parser_message
    global street_number_value
    global street_number_suffix_value
    global street_name_suffix_value
    global subaddress_type_value
    global subaddress_id_value
    global cityNameInput
    global state_name_input
    global zip_code
    global zip_plus_4
    global alesco_address_type
    global full_address_input
    global input_place_name

    # ######################################################################################
    # Get address from input row
    # ######################################################################################

    # Re-init parser status values
    parser_had_issues = False
    custom_parsed = False
    parser_message = ''

    if vverbose:
        print('Source Row: {0}'.format(row))

    # Get optional address parts: Street Number
    if street_number_column != '':
        if street_number_column in input_column_list:
            street_number_index = input_column_list.index(street_number_column)
            street_number_value =row[street_number_index]
            # If street_number is 0 then reset it blank
            if street_number_value == 0:
                street_number_value = ''
        else:
            print('Error. Did not find street_number_column column ' + street_number_column + ' in ' + address_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()
    else:
        street_number_value = ''

    # Get optional address parts: Street Number Suffix
    if street_number_suffix_column != '':
        if street_number_suffix_column in input_column_list:
            street_number_suffix_index = input_column_list.index(street_number_suffix_column)
            street_number_suffix_value =row[street_number_suffix_index]
            if street_number_suffix_value is None:
                street_number_suffix_value = ''
            street_number_suffix_value = street_number_suffix_value.upper()
        else:
            print('Error. Did not find street_number_suffix_column column ' + street_number_suffix_column + ' in ' + address_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()
    else:
        street_number_suffix_value = ''

    # Get Address
    if address_column in input_column_list:
        addressIndex = input_column_list.index(address_column)
    else:
        print('Error. Did not find address_column column ' + address_column + ' in ' + address_table)
        print('Existing columns: {0}'.format(input_column_list))
        exit()
    address_to_parse=row[addressIndex]
    if address_to_parse is None:
        address_to_parse = ''
    address_to_parse = address_to_parse.upper()

    # Get optional address parts: Street Name Suffix
    if street_name_suffix_column != '':
        if street_name_suffix_column in input_column_list:
            street_name_suffix_index = input_column_list.index(street_name_suffix_column)
            street_name_suffix_value =row[street_name_suffix_index]
            if street_name_suffix_value is None:
                street_name_suffix_value = ''
            street_name_suffix_value = street_name_suffix_value.upper()
        else:
            print('Error. Did not find street_name_suffix_column column ' + street_name_suffix_column + ' in ' + address_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()
    else:
        street_name_suffix_value = ''

    # Get optional address parts: Subaddress Type
    subaddress_type_value = ''
    if subaddress_type_column != '':
        if subaddress_type_column in input_column_list:
            subaddress_type_index = input_column_list.index(subaddress_type_column)
            subaddress_type_value =row[subaddress_type_index]
            if subaddress_type_value is None:
                subaddress_type_value = ''
            subaddress_type_value = subaddress_type_value.upper()
        else:
            print('Error. Did not find subaddress_type_column column ' + subaddress_type_column + ' in ' + address_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()
        if not subaddress_type_value == '':
            # Convert any float values to INT (as per legacy DBI data)
            if isinstance(subaddress_type_value, float):
                #print('subaddress_type_value: "{0}"'.format(subaddress_type_value))
                subaddress_type_value = int(subaddress_type_value)
            #print('subaddress_type_value: "{0}"'.format(subaddress_type_value))

    # Get optional address parts: Subaddress Identifier
    if subaddress_id_column != '':
        if subaddress_id_column in input_column_list:
            subaddress_id_index = input_column_list.index(subaddress_id_column)
            subaddress_id_value =row[subaddress_id_index]
            if subaddress_id_value is None:
                subaddress_id_value = ''
            subaddress_id_value = subaddress_id_value.upper()
        else:
            print('Error. Did not find subaddress_id_column column ' + subaddress_id_column + ' in ' + address_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()
        if not subaddress_id_value == '':
            # Convert any float values to INT (as per legacy DBI data)
            if isinstance(subaddress_id_value, float):
                #print('subaddress_id_value: "{0}"'.format(subaddress_id_value))
                subaddress_id_value = int(subaddress_id_value)
            #print('subaddress_id_value: "{0}"'.format(subaddress_id_value))
    else:
        subaddress_id_value = ''

    # If a subaddress_id is present without subaddress_type (i.e. a single number '123' instead of 'UNIT 123')
    # then the parser gets confused.
    # In these cases hardcode the subaddress_type to 'UNIT'.
    if len(subaddress_id_value) and not len(subaddress_type_value):
        print('WARNING: subaddress_type_value is blank but subaddress_id_value is not. Setting subaddress_type_value to "UNIT" so that the parserator routine does not get confused.')
        subaddress_type_value = 'UNIT'

    # Get optional City column
    if city_column != '':
        if city_column in input_column_list:
            city_index = input_column_list.index(city_column)
        else:
            print('Error. Did not find city_column column ' + city_column + ' in ' + address_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()
        cityNameInput=row[city_index]
        if cityNameInput is None:
            cityNameInput = ''
        cityNameInput = cityNameInput.upper()
    else:
        if all_sf == 'yes':
            cityNameInput = DEFAULT_CITY
        else:
            cityNameInput = ''
    input_place_name = cityNameInput

    # Get optional State column
    if state_column != '':
        if state_column in input_column_list:
            state_index = input_column_list.index(state_column)
        else:
            print('Error. Did not find state_column column ' + state_column + ' in ' + address_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()
        state_name_input=row[state_index]
        if state_name_input is None:
            state_name_input = ''
        state_name_input = state_name_input.upper()
    else:
        if all_sf == 'yes':
            state_name_input = DEFAULT_STATE
        else:
            state_name_input = ''

    # Get optional ZIP column
    zip_code = ''
    zip_plus_4 = ''
    if zip_column != '':
        if zip_column in input_column_list:
            zipIndex = input_column_list.index(zip_column)
        else:
            print('Error. Did not find zip_column column ' + zip_column + ' in ' + address_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()
        full_zip_code=row[zipIndex]
        if full_zip_code is None:
            full_zip_code = ''

        zip_code = full_zip_code
        # if len(str(full_zip_code)) == 10:
        #     #TODO: Check that there is just one delimiter
        #     # (some international inputs have more than one delimiter which confuses the set operation below)
        #     #Allow for either - or ' ' as zip/zip4 delimiter
        #     if '-' in full_zip_code:
        #         zip_code,zip_plus_4 = full_zip_code.split('-')
        #     elif ' ' in full_zip_code:
        #         zip_code,zip_plus_4 = full_zip_code.split(' ')

    # TODO: Implement a 'complete_zip' which is either '', 'XXXXX' or 'XXXXX-XXXX'

    # Get optional Alesco 'Address Type'
    if alesco_address_type_column != '':
        if alesco_address_type_column in input_column_list:
            alesco_address_type_index = input_column_list.index(alesco_address_type_column)
            alesco_address_type=row[alesco_address_type_index]
        else:
            print('Error. Did not find alesco_address_type_column column ' + alesco_address_type_column + ' in ' + address_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()
    else:
        alesco_address_type = ''

    # ######################################################################################
    # Concatenate all the address input parts
    # ######################################################################################
    # including optional (and hence possibly blank) things like street number, street number suffix, etc

    # TODO LATER: Refactor variables names with consistent variable suffix values (_column, _index, _name, _value)

    # TODO: Replace zip_code with complete_zip_code

    full_address_input = sfgis_toolbox.remove_duplicate_spaces(' '.join([str(street_number_value), str(street_number_suffix_value), str(address_to_parse), str(street_name_suffix_value), str(subaddress_type_value), str(subaddress_id_value), ',', str(cityNameInput), str(state_name_input), str(zip_code)]))

    # ######################################################################################
    # Parse Address
    # ######################################################################################
    if vverbose:
        print('Parsing full_address_input: {0}'.format(full_address_input))

    parsed_address = invoke_parserator(full_address_input)

    return parsed_address

def process_tuple():

    global parser_had_issues
    global custom_parsed
    global parser_message

    global address_number
    global address_number_suffix
    global street_name
    global street_name_post_type
    global street_name_post_directional
    global subaddress_type
    global subaddress_identifier
    global landmark_name
    global building_name
    global place_name
    global state_name
    global ZipCode
    
    #for addr_tuple in parsed_address: # 'parse' method
    for tuple_name, tuple_value in parsed_address_dict.items(): # 'tag' method

        if vverbose:
            print('tuple_name: {}'.format(tuple_name))
            print('tuple_value: {}'.format(tuple_value))

        # Convert input to upper case
        tuple_value = tuple_value.upper()

        # Remove single quotes
        tuple_value = tuple_value.replace("'", "")

        # Replace comma with space (as opposed to remove comma, in case it was also a delimiter)
        tuple_value = tuple_value.replace(',', '')

        # Replace period with space
        tuple_value = tuple_value.replace('.', '')

        # Remove leading, trailing, duplicate spaces
        tuple_value = sfgis_toolbox.remove_duplicate_spaces(tuple_value)

        #print('tuple_value: {}'.format(tuple_value))
        #exit()

        tuple_string = '{} = {}'
        if vverbose:
            print(tuple_string.format(tuple_name, tuple_value))

        # Address Number
        if tuple_name == 'AddressNumber':
            address_number = tuple_value
            #print('**  address_number: {0}'.format(address_number))

            # The parserator does not separate address_number_suffix from the address_number.
            # So manually separate them with regex
            # e.g. '1000A' becomes '1000' and 'A'
            r = re.compile("([0-9]*)([a-zA-Z]*)")
            m = r.match(address_number)

            if m:
                g = m.groups()
            else:
                print('Expecting m to have groups. m={0}'.format(m))
                t = type(m)
                print('type: ', t)
                exit()

            if len(g) == 2:
                address_number = g[0]
                address_number_suffix = g[1]

                #if address_number_suffix:
                    #print('address_number: {0}'.format(address_number))
                    #print('address_number_suffix: {0}'.format(address_number_suffix))
                    
            else:
                print('Expecting groups to have length of 2. Instead, the length is {0}'.format(len(g)))
                print('address_number: {0}'.format(address_number))
                exit()

            # if not address_number_suffix == '':
            #     print('address_number: {0}'.format(address_number))
            #     print('address_number_suffix: {0}'.format(address_number_suffix))
            #     exit()

            # else:
            #     print('a-okay')
            #     print('address_number: {0}'.format(address_number))
            #     print('address_number_suffix: {0}'.format(address_number_suffix))

        elif tuple_name == 'AddressNumberSuffix':
            address_number_suffix = ' '.join([str(address_number_suffix), str(tuple_value)])

        # Street Name
        elif tuple_name == 'StreetNamePreDirectional':
            # The EAS does not have any streets with street_name_pre_directional.
            # So change the value to street_name
            street_name = ' '.join([str(street_name_pre_directional), str(tuple_value)])
            #street_name_pre_directional = ' '.join([str(street_name_pre_directional), str(tuple_value)])

        elif tuple_name == 'StreetNamePreType':
            # The EAS does not have any streets with street_name_pre_type.
            # So change the value to street_name
            street_name = ' '.join([str(street_name), str(tuple_value)])
            #street_name_pre_type = ' '.join([str(street_name_pre_type), str(tuple_value)])
        elif tuple_name == 'StreetName':
            street_name = ' '.join([str(street_name), str(tuple_value)])
        elif tuple_name == 'StreetNamePostType':
            street_name_post_type = ' '.join([str(street_name_post_type), str(tuple_value)])
        elif tuple_name == 'StreetNamePostDirectional':
            street_name_post_directional = ' '.join([str(street_name_post_directional), str(tuple_value)])

        # Subaddress Type (Combine with Occupancy Type so we don't have a mix of both)
        elif tuple_name == 'SubaddressType' or tuple_name == 'OccupancyType':
            subaddress_type = ' '.join([str(subaddress_type), str(tuple_value)])
            subaddress_type = sfgis_toolbox.remove_duplicate_spaces(subaddress_type)

        # If we encounter 'IntersectionSeparator' then we have a problem. 
        # Take those values and append them to the subaddress identifier
        elif tuple_name == 'IntersectionSeparator':
            subaddress_type = ' '.join([str(subaddress_type), str(tuple_value)])
            subaddress_type = sfgis_toolbox.remove_duplicate_spaces(subaddress_type)

            parser_message += ' Unexpected part: ' + tuple_name + ' :' + tuple_value
            parser_message += ' :::::::: full_address_input=' + full_address_input
            #parser_message += ' :::::::: parsed_address=' + str(parsed_address)
            parser_had_issues = True
            custom_parsed = True

            print('parser_message: {0}'.format(parser_message))
            print('street_name: {0}'.format(street_name))
            # exit()

        # If we encounter 'LandmarkName' then save it for later
        elif tuple_name == tuple_name == 'LandmarkName':
            landmark_name = ' '.join([str(landmark_name), str(tuple_value)])

            parser_message += ' Unexpected part: ' + tuple_name + ' :' + tuple_value
            
            parser_message += ' :::::::: full_address_input=' + full_address_input
            #parser_message += ' :::::::: parsed_address=' + str(parsed_address)

            parser_had_issues = True
            custom_parsed = True

        # Subaddress Identifier
        elif tuple_name == 'SubaddressIdentifier' or tuple_name == 'OccupancyIdentifier':
            subaddress_identifier = ' '.join([str(subaddress_identifier), str(tuple_value)])
            #print('subaddress_identifier: "{0}"'.format(subaddress_identifier))

        # Subaddress - BuildingName
        elif tuple_name == 'BuildingName':
            building_name = ' '.join([str(building_name), str(tuple_value)])

        # City/State/Zip
        elif tuple_name == 'PlaceName':
            # Append place_name to get 'SAN' and 'FRANCISCO' in same variable
            place_name = ' '.join([str(place_name), str(tuple_value)])
            #print('place_name: {0}'.format(place_name))

        elif tuple_name == 'StateName':
            state_name = ' '.join([str(state_name), str(tuple_value)])

        elif tuple_name == 'ZipCode':
            ZipCode = ' '.join([str(ZipCode), str(tuple_value)])

        elif tuple_name == 'ZipPlus4':
            ZipCode = ' '.join([str(ZipCode), str('-{}'.format(tuple_value))])
        # PO Box
        elif tuple_name == 'USPSBoxType':
            subaddress_identifier = ' '.join([str(subaddress_identifier), str(tuple_value)])
            #USPSBoxType = ' '.join([str(USPSBoxType), str(tuple_value)])
        elif tuple_name == 'USPSBoxID':
            subaddress_identifier = ' '.join([str(subaddress_identifier), str(tuple_value)])
            #USPSBoxID = ' '.join([str(USPSBoxID), str(tuple_value)])

        elif tuple_name == 'StreetNamePreModifier':
            if tuple_value == 'EL' or tuple_value == 'OLD' or tuple_value == 'ORA':
                # Put the value in front of the street name
                parser_had_issues = True
                custom_parsed = True
                street_name = ' '.join([str(tuple_value), str(street_name)])
            else:
                print('Unknown part: ' + tuple_name + ' :', end='')
                print(tuple_value)
                print('Source Row: {0}'.format(row))
                parser_had_issues = True
                parser_message = 'Unknown part: ' + tuple_name + ' :' + tuple_value
                parser_message += ' :::::::: full_address_input=' + full_address_input
                #parser_message += ' :::::::: parsed_address=' + str(parsed_address)
                exit()

        else:
            print('Unknown part: ' + tuple_name + ' :', end='')
            print(tuple_value)
            print('Source Row: {0}'.format(row))
            parser_had_issues = True
            parser_message = 'Unknown part: ' + tuple_name + ' :' + tuple_value
            parser_message += ' :::::::: full_address_input=' + full_address_input
            #parser_message += ' :::::::: parsed_address=' + str(parsed_address)
            #exit()

def trim_remove_duplicate_spaces():

    global address_number_suffix
    global street_name_pre_directional
    global street_name_pre_type
    global street_name
    global street_name_post_type
    global street_name_post_directional
    global subaddress_type
    global subaddress_identifier
    global occupancy_type
    global occupancy_identifier
    global building_name
    global landmark_name
    global place_name
    global state_name
    global zip_code
    global zip_plus_4

    # ######################################################################################
    # Trim leading and trailing spaces
    # ######################################################################################
    address_number_suffix = sfgis_toolbox.remove_duplicate_spaces(address_number_suffix)
    street_name_pre_directional = sfgis_toolbox.remove_duplicate_spaces(street_name_pre_directional)
    street_name_pre_type = sfgis_toolbox.remove_duplicate_spaces(street_name_pre_type)
    street_name = sfgis_toolbox.remove_duplicate_spaces(street_name)
    street_name_post_type = sfgis_toolbox.remove_duplicate_spaces(street_name_post_type)
    street_name_post_directional = sfgis_toolbox.remove_duplicate_spaces(street_name_post_directional)
    subaddress_type = sfgis_toolbox.remove_duplicate_spaces(subaddress_type)
    subaddress_identifier = sfgis_toolbox.remove_duplicate_spaces(subaddress_identifier)
    occupancy_type = sfgis_toolbox.remove_duplicate_spaces(occupancy_type)
    occupancy_identifier = sfgis_toolbox.remove_duplicate_spaces(occupancy_identifier)
    building_name = sfgis_toolbox.remove_duplicate_spaces(building_name)
    landmark_name = sfgis_toolbox.remove_duplicate_spaces(landmark_name)
    place_name = sfgis_toolbox.remove_duplicate_spaces(place_name)
    state_name = sfgis_toolbox.remove_duplicate_spaces(state_name)
    zip_code = sfgis_toolbox.remove_duplicate_spaces(zip_code)
    zip_plus_4 = sfgis_toolbox.remove_duplicate_spaces(zip_plus_4)

def invoke_parserator(address):
    global parser_input
    parser_input = address
    try:
        parsed_address = '' # Init to null
        parsed_address = usaddress.tag(address)
    except:
        print('======================================================================')
        print('Error in "tag" method of "usaddress" on address: ' + full_address_input)
        if not parsed_address == '':
            print('parsed_address: {0}'.format(parsed_address))
        print('======================================================================')
        print('SKIPPING THIS RECORD')
    finally:
        return parsed_address

def standardize_address_parts():

    global parser_had_issues
    global custom_parsed
    global parser_message

    global address_number
    global address_number_suffix
    global street_name_pre_type
    global street_name
    global street_name_post_type
    global street_name_post_directional
    global subaddress_type
    global subaddress_identifier
    global landmark_name

    # If street name is single-digit street then prefix with '0' (e.g. 1st -> 01st)
    # TODO: Account for cases where input is lowercase instead of assuming it will be upper case.

    name_list = ['1ST','2ND','3RD','4TH','5TH','6TH','7TH','8TH','9TH']

    if street_name in name_list:
        #print ('BEFORE: street_name={0}'.format(street_name))
        street_name = '0' + street_name
        custom_parsed = True
        parser_message += ' Prefix single-digit street name with 0.'
        #print ('AFTER: street_name={0}'.format(street_name))

    # If street_name is 'BAYSHORE' then reset to 'BAY SHORE' (space between words)
    if street_name == 'BAYSHORE':
        street_name = 'BAY SHORE'
        custom_parsed = True
        parser_message += ' Converted BAYSHORE to BAY SHORE.'

    # If street_name is 'FORT MASON' then parse manually
    if street_name == 'FORT MASON':
        # Get and parse the address_number from the landmark_name

        if not landmark_name == '':
            parsed_landmark_name = landmark_name.split(' ')
            first_landmark_name = parsed_landmark_name[0]
            #print('first_landmark_name: {0}'.format(first_landmark_name))

            # See if first_landmark_name is an int. If so use it for the address_number.
            if isinstance(first_landmark_name, int):
                address_number = first_landmark_name
            # Otherwise parse the number and letter out of first_landmark_name
            else:
                address_number = re.findall('\d+', first_landmark_name)[0]
                #print('address_number: {0}'.format(address_number))

                #Now see if there are letter after the number
                parsed_address_number = first_landmark_name.split(address_number)
                #print('parsed_address_number: {0}'.format(parsed_address_number))
                address_number_suffix = parsed_address_number[1]
                #print('address_number_suffix: {0}'.format(address_number_suffix))
                
            # Re-set landmark name to blank
            landmark_name = ''

            parser_had_issues = True
            custom_parsed = True

        #print('address_number: {0}'.format(address_number))
                
        # Check the LandmarkName
        #print('landmark_name: {0}'.format(landmark_name))

    # If landmark_name is set then handle it.
    if not landmark_name == '':
        print('landmark_name: {0}'.format(landmark_name))
        print('landmark_name!')
        print('full_address_input')
        # TODO: Handle landmarks
        # For now: set it to streetname
        street_name = ' '.join([str(street_name), str(landmark_name)])

        print('TODO: Handle landmark_name. full_address_input: {0}'.format(full_address_input))
        print('TODO: Handle landmark_name. parsed_address: {0}'.format(parsed_address))

        #exit()

    # If street_name_pre_type is 'AVENUE' then parse manually
    if street_name_pre_type == 'AVENUE':
        street_name_pre_type = ''
        street_name = 'AVENUE {0}'.format(street_name)
        parser_had_issues = True
        custom_parsed = True

    # ########################################################################
    # If street_name_post_type is 'FL' then manually set subaddress_type
    # ########################################################################
    if street_name_post_type == 'FL':
        subaddress_type = 'FL'
        street_name_post_type = ''
        parser_had_issues = True
        custom_parsed = True

    # ########################################################################
    # See if the subaddress_identifier is two words long.
    # If so then see if the first word is a common subaddress_type
    # If so then separate the value into subaddress_type and subaddress_identifier
    # ########################################################################
    parsed_subaddress_identifier = subaddress_identifier.split(' ')
    if len(parsed_subaddress_identifier) == 2:
        first_subaddress_identifier = parsed_subaddress_identifier[0]
        second_subaddress_identifier = parsed_subaddress_identifier[1]

        #print('first_subaddress_identifier: "{0}"'.format(first_subaddress_identifier))
        #print('second_subaddress_identifier: "{0}"'.format(second_subaddress_identifier))

        if first_subaddress_identifier == 'APT':
            subaddress_type = 'APT'
            subaddress_identifier = second_subaddress_identifier
            parser_had_issues = True
            custom_parsed = True
        elif first_subaddress_identifier == 'FL':
            subaddress_type = 'FL'
            subaddress_identifier = second_subaddress_identifier
            parser_had_issues = True
            custom_parsed = True
        elif first_subaddress_identifier == 'PH':
            subaddress_type = 'PH'
            subaddress_identifier = second_subaddress_identifier
            parser_had_issues = True
            custom_parsed = True
        elif first_subaddress_identifier == 'STE':
            subaddress_type = 'STE'
            subaddress_identifier = second_subaddress_identifier
            parser_had_issues = True
            custom_parsed = True
        elif first_subaddress_identifier == 'STR':
            subaddress_type = 'STR'
            subaddress_identifier = second_subaddress_identifier
            parser_had_issues = True
            custom_parsed = True
        elif first_subaddress_identifier == 'UNIT':
            subaddress_type = 'UNIT'
            subaddress_identifier = second_subaddress_identifier
            parser_had_issues = True
            custom_parsed = True
        elif first_subaddress_identifier == '#':
            subaddress_type = '#'
            subaddress_identifier = second_subaddress_identifier
            parser_had_issues = True
            custom_parsed = True
        
        #print('subaddress_identifier: "{0}"'.format(subaddress_identifier))

    # ########################################################################
    # See if the subaddress_identifier is 'W' and the subaddress_type is blank.
    # If so then set the street_name_post_direction to 'W' and subaddress_identifier to blank
    # If so then separate the value into subaddress_type and subaddress_identifier
    # ########################################################################
    if subaddress_identifier == 'W' and subaddress_type == '':
        street_name_post_directional = 'WEST'
        subaddress_identifier = ''
        parser_had_issues = True
        custom_parsed = True

def evaluate_and_correct_place_name():
    global parser_had_issues
    global custom_parsed
    global parser_message

    global street_name_pre_type
    global street_name
    global street_name_post_type
    global subaddress_identifier
    global subaddress_type
    global place_name
    global state_name
    global input_place_name
    global city_column
    global all_sf
    global full_address_input

    # See if city_name argument was passed.
    # If so then compare the input value with the parsed value
    # If not the same then parse the city name out of the value 
    # and use the leftover for subaddress_identifier
    if city_column != '' and input_place_name != '':
        if not input_place_name == place_name and not place_name == DEFAULT_CITY:
            print('input_place_name:"{}"'.format(input_place_name))
            print('      place_name:"{}"'.format(place_name))
            # Save anything before the city name to subaddress_identifier
            parsed_place_name = place_name.split(input_place_name)
            print('parsed_place_name: {}'.format(parsed_place_name))
            unit_id = parsed_place_name[0]
            print('unit_id:"{}"'.format(unit_id))
            # Trim the parsed unit_id
            unit_id = sfgis_toolbox.remove_duplicate_spaces(unit_id)
            # Append the parsed unit_id to the subaddress_identifier
            subaddress_identifier = ' '.join([str(subaddress_identifier), str(unit_id)])

            # Reset place_name back to input_place_name
            place_name = input_place_name

            # Set flag show we parsed manually
            custom_parsed = True
            parser_had_issues = True
            parser_message += ' Reset parsed place_name back to raw value:"' + place_name + '". '
            parser_message += ' Appended subaddress_identifier with characters incorrectly parsed as place_name:"' + unit_id + '". '
            parser_message += ' :::::::: full_address_input=' + full_address_input
            print('      parser_message:"{}"'.format(parser_message))

    if all_sf == 'yes':

        # Reset city and state
        if not place_name == DEFAULT_CITY:
            if vverbose:
                print('Resetting place_name from "{}" to "{}"'.format(place_name, DEFAULT_CITY))
            print('- - - - - - - - -')
            print('parser_input:{}'.format(parser_input))
            print('Resetting place_name from "{}" to "{}"'.format(place_name, DEFAULT_CITY))
            parser_message += ' Reset parsed place_name from :"' + place_name + '". to default value:"' + DEFAULT_CITY + '". '
            custom_parsed = True
            parser_had_issues = True
            place_name = DEFAULT_CITY
            # TODO Append to unit value: the text truncated from the place_name

        if not state_name == DEFAULT_STATE:
            if vverbose:
                print('Resetting state_name from "{}" to "{}"'.format(state_name, DEFAULT_STATE))
            parser_message += ' Reset parsed state_name from :"' + state_name + '". to default value:"' + DEFAULT_STATE + '". '
            custom_parsed = True
            parser_had_issues = True
            state_name = DEFAULT_STATE

def standardize_eas_fields():

    global street_name_pre_directional
    global street_name_post_type
    global street_name_post_directional

    # ########################################################################
    # Standardize EAS fields
    # ########################################################################
    # TODO: This step is no longer needed because we already change any street_name_pre_directional value into street_name.
    street_name_pre_directional_eas, error_flag = standardize_to_eas('street_name_pre_directional', street_name_pre_directional)

    if error_flag:
        print('Error 1 in standardize_to_eas: street_name_pre_directional_eas')
        print('full_address_input: {0}'.format(full_address_input))
        print('parsed_address: {0}'.format(parsed_address))

    if not street_name_pre_directional_eas == street_name_pre_directional:
        #print('street_name_pre_directional: "{0}"; street_name_pre_directional_eas "{1}"'.format(street_name_pre_directional, street_name_pre_directional_eas))
        street_name_pre_directional = street_name_pre_directional_eas

    street_name_post_type_eas, error_flag = standardize_to_eas('street_name_post_type', street_name_post_type)

    if error_flag:
        print('Error 2 in standardize_to_eas: street_name_post_type_eas')
        print('full_address_input: {0}'.format(full_address_input))
        print('parsed_address: {0}'.format(parsed_address))

    if not street_name_post_type_eas == street_name_post_type:
        #print('street_name_post_type: "{0}"; street_name_post_type_eas "{1}"'.format(street_name_post_type, street_name_post_type_eas))
        street_name_post_type = street_name_post_type_eas

    street_name_post_directional_eas, error_flag = standardize_to_eas('street_name_post_directional', street_name_post_directional)

    if error_flag:
        print('Error 3 in standardize_to_eas: street_name_post_directional_eas')
        print('full_address_input: {0}'.format(full_address_input))
        print('parsed_address: {0}'.format(parsed_address))

    if not street_name_post_directional_eas == street_name_post_directional:
        #print('street_name_post_directional: "{0}"; street_name_post_directional_eas "{1}"'.format(street_name_post_directional, street_name_post_directional_eas))
        street_name_post_directional = street_name_post_directional_eas

def standardize_to_eas(field, value):

    # Init error flag to flase
    error_flag = False

    # Convert to upper case
    value = value.upper()

    if value == '':
        return value, error_flag
    if field == 'street_name_pre_directional' or field == 'street_name_post_directional':
        if value == 'EAST' or value =='E':
            new_value = 'EAST'
        elif value == 'NORTH' or value =='N' or value =='NO':
            new_value = 'NORTH'
        elif value == 'SOUTH' or value =='S':
            new_value = 'SOUTH'
        elif value == 'WEST' or value =='W':
            new_value = 'WEST'
        else:
            print('Error standardize_to_eas: Unknown value "{0}" in field "{1}"'.format(value, field))
            new_value = value
            error_flag = True

    elif field == 'street_name_post_type':
        if value == 'ALY' or value =='AL' or value == 'ALLEY':
            new_value = 'ALY'
        elif value == 'AVE' or value =='AV' or value == 'AVENUE':
            new_value = 'AVE'
        elif value == 'BLVD' or value =='BL' or value == 'BOULEVARD':
            new_value = 'BLVD'
        elif value == 'CIR' or value =='CR':
            new_value = 'CIR'
        elif value == 'CT' or value =='COURT':
            new_value = 'CT'
        elif value =='CTR' or value =='CENTER':
            new_value = 'CTR'
        elif value == 'DR' or value == 'DRIVE':
            new_value = 'DR'
        elif value == 'HL' or value =='HILL' or value =='HLS':
            new_value = 'HL'
        elif value == 'HWY' or value =='HY':
            new_value = 'HWY'
        elif value == 'LN' or  value == 'LANE':
            new_value = 'LN'
        elif value == 'LOOP':
            new_value = 'LOOP'
        elif value == 'PARK' or value =='PK':
            new_value = 'PARK'
        elif value == 'PLZ' or value =='PZ' or value == 'PLAZA':
            new_value = 'PLZ'
        elif value == 'PL' or  value == 'PLACE':
            new_value = 'PL'
        elif value == 'RD':
            new_value = 'RD'
        elif value == 'ROW':
            new_value = 'ROW'
        elif value == 'SQ':
            new_value = 'SQ'
        elif value == 'ST' or value =='STREET':
            new_value = 'ST'
        elif value == 'TER' or value =='TR' or value == 'TERRACE':
            new_value = 'TER'
        elif value == 'WALK':
            new_value = 'WALK'
        elif value == 'WAY' or value =='WY':
            new_value = 'WAY'
        else:
            #print('Error standardize_to_eas: Unknown value "{0}" in field "{1}"'.format(value, field))
            new_value = value
            error_flag = True
    else:
        print('Error standardize_to_eas: Unknown field "{0}" (value= "{1}")'.format(field, value))
        new_value = value
        error_flag = True
        exit()

    #if not value == new_value:
    #    print('value: "{0}"; new_value "{1}"'.format(value, new_value))

    return new_value, error_flag

def init_address_parts():
    global address_number
    global address_number_suffix
    global street_name_pre_directional
    global street_name_pre_type
    global street_name
    global street_name_post_type
    global street_name_post_directional
    global subaddress_type
    global subaddress_identifier
    global occupancy_type
    global occupancy_identifier
    global building_name
    global landmark_name

    global parserator_tag

    global place_name
    global state_name
    global ZipCode

    # ######################################################################################
    # Init address parts
    # ######################################################################################
    address_number = ''
    address_number_suffix = ''
    street_name_pre_directional = ''
    street_name_pre_type = ''
    street_name = ''
    street_name_post_type = ''
    street_name_post_directional = ''
    subaddress_type = ''
    subaddress_identifier = ''
    occupancy_type = ''
    occupancy_identifier = ''
    building_name = ''
    landmark_name = ''

    parserator_tag = ''

    place_name = ''
    state_name = ''
    ZipCode = ''

def combine_address_parts():

    global complete_address_number
    global complete_street_name
    global complete_subaddress
    global complete_place_name
    global delivery_address_without_unit
    global delivery_address_with_unit

    # ########################################################################
    # Combine address parts
    # ########################################################################

    # Set the "Complete Address Number", complete_address_number
    # From the FGDC: "An Address Number, alone or with an Address Number Prefix and/or Address Number Suffix"
    # See https://www.fgdc.gov/standards/projects/address-data/index_html
    complete_address_number = sfgis_toolbox.remove_duplicate_spaces(' '.join([address_number,address_number_suffix]))

    # Set the "Complete Street Name", complete_street_name
    # From the FGDC: "A "Complete Street Name is composed from eight simple elements, which, if used, must appear in the following order: Street Name Pre Modifier, Street Name Pre Directional, Street Name Pre Type, Separator Element, Street Name, Street Name Post Type, Street Name Post Directional, and Street Name Post Modifier. Each of these elements is defined and described elsewhere in the standard."
    # See https://www.fgdc.gov/standards/projects/address-data/index_html
    complete_street_name = sfgis_toolbox.remove_duplicate_spaces(' '.join([street_name_pre_directional, street_name_pre_type, street_name, street_name_post_type,  street_name_post_directional]))

    # Set the "Complete Subaddress", complete_subaddress
    # From the FGDC: ""
        # One or more Subaddress Elements that identify a subaddress within an addressed feature. A subaddress is a separate, identifiable portion of a feature, the whole of which is identified by a:
        # • Complete Address Number and Complete Street Name (in the case of a Numbered Thoroughfare Address)
        # • Two Complete Address Numbers, separated by a hyphen, and followed by a Complete Street Name (in the case of a Two Number Address Range)
        # • Complete Street Name (in the case of an Unnumbered Thoroughfare Address)
        # • Complete Landmark Name (in the case of a Landmark Address)
        # 81
        # Federal Geographic Data Committee FGDC Document Number FGDC-STD-016-2011 United States Thoroughfare, Landmark, and Postal Address Data Standard
        # ￼
        # • Complete Address Number and Complete Landmark Name or Complete Place Name (in the case of a Community Address)
        # • USPS Box or USPS Address (in the case of a USPSPostal Delivery Box or USPSPostal Delivery Route address; for these classes, PMB (private mail box) is the only Subaddress Type permitted.)
    # See https://www.fgdc.gov/standards/projects/address-data/index_html

    # Order is from biggest part of structure to smallest and/or from geographic order when physically going to the location.
    # See FGDC "Usually Complete Subaddresses follow a pattern of Building- Floor-Room (or Doorway), but due to the wide variety of cases no general rule can be given. In composing the Complete Subaddress, the Subaddress Elements should be ordered from largest to smallest, or in the order one would encounter them in navigating from outside the site to the designated subaddress. If desired, use the Element Sequence Number to indicate the sequence in which the Subaddress Elements should be ordered."
    complete_subaddress = sfgis_toolbox.remove_duplicate_spaces(' '.join([building_name, subaddress_type, subaddress_identifier, occupancy_type, occupancy_identifier]))
    
    # Set the "Complete Place Name", complete_place_name
    # From the FGDC "One or more Place Names which identify an area, sector, or development (such as a neighborhood or subdivision in a city, or a rural settlement in unincorporated area); incorporated municipality or other general-purpose local governmental unit; county; or region within which the address is physically located; or the name given by the U.S. Postal Service to the post office from which mail is delivered to the address.""
    # See https://www.fgdc.gov/standards/projects/address-data/index_html
    complete_place_name = place_name

    # Set the "Delivery Address", DeliveryAddress
    # From the FGDC "The entire address, unparsed, except for the Place Name, State Name, Zip Code, Zip Plus 4, Country Name, and, optionally, Complete Subaddress elements."
    # See https://www.fgdc.gov/standards/projects/address-data/index_html
    # Set the "Address Without Unit", AddressWithoutUnit
    # Custom SFGIS column. Street number and address but no unit.
    delivery_address_without_unit = sfgis_toolbox.remove_duplicate_spaces(' '.join([complete_address_number,complete_street_name]))

    delivery_address_with_unit = sfgis_toolbox.remove_duplicate_spaces(' '.join([delivery_address_without_unit,complete_subaddress]))
    # delivery_address_with_unit = delivery_address_without_unit +' ' + complete_subaddress
    # delivery_address_with_unit = sfgis_toolbox.remove_duplicate_spaces(delivery_address_with_unit)

# ######################################################################################
# Connect to database
# ######################################################################################

try:
    connection = psycopg2.connect(host=odbc_server, database=odbc_database, user=odbc_uid, password=odbc_pwd)

    cur = connection.cursor()

    # ######################################################################################
    # BEGIN: Create or alter destination table
    # ######################################################################################

    sql = 'DROP TABLE IF EXISTS ' + output_table
    cur.execute(sql)
    connection.commit()

    sql = 'CREATE TABLE ' + output_table + ' (id SERIAL NOT NULL PRIMARY KEY'
    sql += ", address_to_parse VARCHAR(255) NOT NULL DEFAULT ''"
    #sql = 'CREATE TABLE ' + output_table + ' ( '
    #sql += " address_to_parse VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ', fk_id BIGINT NOT NULL DEFAULT 0'
    sql += ', address_number BIGINT NOT NULL DEFAULT 0'
    sql += ", address_number_suffix VARCHAR(255) NOT NULL DEFAULT ''"

    sql += ", street_name_pre_directional VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ", street_name_pre_type VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ", street_name VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ", street_name_post_type VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ", street_name_post_directional VARCHAR(255) NOT NULL DEFAULT ''"

    sql += ", subaddress_type VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ", subaddress_identifier VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ", occupancy_type VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ", occupancy_identifier VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ", building_name VARCHAR(255) NOT NULL DEFAULT ''"

    sql += ", place_name VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ", state_name VARCHAR(255) NOT NULL DEFAULT ''"

    sql += ", zip_code VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ", zip_plus_4 VARCHAR(255) NOT NULL DEFAULT ''"

    sql += ", complete_address_number VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ", complete_street_name VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ", complete_subaddress VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ", complete_place_name VARCHAR(255) NOT NULL DEFAULT ''"

    sql += ", delivery_address_with_unit VARCHAR(255) NOT NULL DEFAULT ''"
    sql += ", delivery_address_without_unit VARCHAR(255) NOT NULL DEFAULT ''"

    sql += ", alesco_address_type VARCHAR(255) NOT NULL DEFAULT ''"

    sql += ", parsed_address VARCHAR(1000) NOT NULL DEFAULT ''"
    sql += ', custom_parsed BOOLEAN DEFAULT NULL'
    sql += ', parser_had_issues BOOLEAN DEFAULT NULL'
    sql += ', modified_by_cpc BOOLEAN DEFAULT NULL'

    sql += ", parser_message VARCHAR(2000) NOT NULL DEFAULT ''"
    sql += ", parserator_tag VARCHAR(255) NOT NULL DEFAULT ''"

    sql += ", address_number_parity VARCHAR(255) NOT NULL DEFAULT ''"

    #sql += ', found_street_name INT NOT NULL DEFAULT -1'

    sql += ')'

    print('sql: {}'.format(sql))
    cur.execute(sql)
    connection.commit()

    # ######################################################################################
    # END: Create or alter destination table
    # ######################################################################################

    # ######################################################################################
    # Get record count
    # ######################################################################################
    sql = 'SELECT COUNT(*) AS count FROM ' + address_table
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    input_record_count = row[0]
    print('input_record_count: {0}'.format(input_record_count))

    # ######################################################################################
    # Get source records
    # ######################################################################################
    sql = 'SELECT * FROM ' + address_table
    #sql += " WHERE address LIKE '%AVENUE%'"
    # sql += ' ORDER BY sfgisgapid '
    sql += limit_source
    cur.execute(sql)
    results = 'true'

    # Echo input column names
    input_column_list = [desc[0] for desc in cur.description]
    if vverbose:
        print('Source Columns: {0}'.format(input_column_list))

    rows = cur.fetchall()
    selected_input_record_count = len(rows)

    for row in rows:

        if vverbose:
            print('Source Row: {0}'.format(row))

        counter+=1

        if counter == 1:
            first_input_row = row
            if vverbose:
                print('First Source Row: {0}'.format(first_input_row))

        # ######################################################################################
        # Get PK field and value
        # ######################################################################################
        if primary_key in input_column_list:
            pk_index = input_column_list.index(primary_key)
            pk_value =row[pk_index]
            if vverbose:
                print('pk_value: {}'.format(pk_value))
        else:
            print('Error. Did not find primary key column ' + primary_key + ' in ' + address_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()

        # ######################################################################################
        # Parse the address
        # ######################################################################################
        parser_input = ''
        parser_message = ''

        parsed_address = parse_address(row)

        # BEGIN Process Parsed Address
        if vverbose:
            print('parsed_address: {}'.format(parsed_address))

        if parsed_address == '':
            print('skipping address in row: {}'.format(row))
            record_skip_count += 1
            continue

        parsed_address_dict = parsed_address[0]
        if vverbose:
            for key, value in parsed_address_dict.items():
                print ('key: ',key, ' ; value: ', value)

        # Store sample results for report
        if counter == 1:
            first_address_parsed = full_address_input
            first_parsed_address = parsed_address

        custom_parsed = False

        init_address_parts()
        process_tuple()
        trim_remove_duplicate_spaces()
        # evaluate_and_correct_place_name()
        # standardize_address_parts()
        # standardize_eas_fields()
        combine_address_parts()

        # END Process Parsed Address



        # ########################################################################
        # Increment counts of parser errors or custom fixes:
        # ########################################################################
        if custom_parsed:
            custom_parse_count += 1
        if parser_had_issues:
            parser_issue_count += 1

        if not parsed_address == '':

            # ######################################################################################
            # Get the 'tag' as found by the parserator usaddress 'tag' method
            # ######################################################################################
            # See http://usaddress.readthedocs.io/en/latest/
            parserator_tag = parsed_address[1] 
            if vverbose:
                print('Parserator "Tag": {0}'.format(parserator_tag))
            if parserator_tag == 'Street Address':
                if vverbose:
                    print('address_tag : ' + parserator_tag)
            elif parserator_tag == 'Ambiguous':
                if vverbose:
                    print('address_tag : ' + parserator_tag)
            elif parserator_tag == 'PO Box':
                if vverbose:
                    print('address_tag : ' + parserator_tag)
            elif parserator_tag == 'Intersection':
                if vverbose:
                    print('address_tag : ' + parserator_tag)
            else:
                print('Unknown address_tag: ' + parserator_tag)
                # parser_had_issues = True
                # parser_message += ' ; Unknown address_tag: ' + parserator_tag

            if vverbose:
                print('-------------------------------')

            # Make sure address_number is an int
            validated_address_number = sfgis_toolbox.validate_integer(address_number)

            # Set address_number parity
            address_number_parity = sfgis_toolbox.get_parity(validated_address_number)

            # ######################################################################################
            # Remove trailing spaces and duplicate spaces from all address parts
            # And change to upper case
            # ######################################################################################
            # TODO: No need to run upper() because we run it from the top of the loop
            address_number_suffix = sfgis_toolbox.remove_duplicate_spaces(address_number_suffix).upper()
            street_name_pre_directional = sfgis_toolbox.remove_duplicate_spaces(street_name_pre_directional).upper()
            street_name_pre_type = sfgis_toolbox.remove_duplicate_spaces(street_name_pre_type).upper()
            street_name = sfgis_toolbox.remove_duplicate_spaces(street_name).upper()
            street_name_post_type = sfgis_toolbox.remove_duplicate_spaces(street_name_post_type).upper()
            street_name_post_directional = sfgis_toolbox.remove_duplicate_spaces(street_name_post_directional).upper()
            subaddress_type = sfgis_toolbox.remove_duplicate_spaces(subaddress_type).upper()
            subaddress_identifier = sfgis_toolbox.remove_duplicate_spaces(subaddress_identifier).upper()
            occupancy_type = sfgis_toolbox.remove_duplicate_spaces(occupancy_type).upper()
            occupancy_identifier = sfgis_toolbox.remove_duplicate_spaces(occupancy_identifier).upper()
            building_name = sfgis_toolbox.remove_duplicate_spaces(building_name).upper()
            place_name = sfgis_toolbox.remove_duplicate_spaces(place_name).upper()
            state_name = sfgis_toolbox.remove_duplicate_spaces(state_name).upper()
            zip_code = sfgis_toolbox.remove_duplicate_spaces(zip_code)
            zip_plus_4 = sfgis_toolbox.remove_duplicate_spaces(zip_plus_4)
            complete_address_number = sfgis_toolbox.remove_duplicate_spaces(complete_address_number).upper()
            complete_street_name = sfgis_toolbox.remove_duplicate_spaces(complete_street_name).upper()
            complete_subaddress = sfgis_toolbox.remove_duplicate_spaces(complete_subaddress).upper()
            complete_place_name = sfgis_toolbox.remove_duplicate_spaces(complete_place_name).upper()
            delivery_address_without_unit = sfgis_toolbox.remove_duplicate_spaces(delivery_address_without_unit).upper()
            delivery_address_with_unit = sfgis_toolbox.remove_duplicate_spaces(delivery_address_with_unit).upper()

            # ######################################################################################
            # Insert parsed values into the output table. Also insert input values.
            # ######################################################################################

            fk_id = pk_value

            if vverbose:
                print('place_name: {0}'.format(place_name))
                print('state_name: {0}'.format(state_name))
                print('zip_code: {0}'.format(zip_code))
                print('zip_plus_4: {0}'.format(zip_plus_4))
                print('complete_address_number: {0}'.format(complete_address_number))
                print('complete_street_name: {0}'.format(complete_street_name))
                print('complete_subaddress: {0}'.format(complete_subaddress))
                print('complete_place_name: {0}'.format(complete_place_name))
                print('delivery_address_without_unit: {0}'.format(delivery_address_without_unit))
                print('delivery_address_with_unit: {0}'.format(delivery_address_with_unit))
                print('address_number_parity: {0}'.format(address_number_parity))
                print('parsed_address: {0}'.format(parsed_address))
                print('fk_id: {0}'.format(fk_id))

            # TODO: reinstate parsed_address after converting "OrderedDict" in parsed_address to text
            parsed_address = ''

            # Add the record to the destination table
            try:

                cur.execute('INSERT INTO ' + output_table + ' (fk_id, parser_had_issues, modified_by_cpc, custom_parsed, parser_message, address_to_parse, alesco_address_type, parserator_tag, address_number, address_number_suffix, street_name_pre_directional, street_name_pre_type, street_name, street_name_post_type, street_name_post_directional, subaddress_type, subaddress_identifier, occupancy_type, occupancy_identifier, building_name, place_name, state_name, zip_code, zip_plus_4, complete_address_number, complete_street_name, complete_subaddress, complete_place_name, delivery_address_without_unit, delivery_address_with_unit, address_number_parity, parsed_address) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)', (fk_id, parser_had_issues, modified_by_cpc, custom_parsed, parser_message, full_address_input, alesco_address_type, parserator_tag, validated_address_number, address_number_suffix, street_name_pre_directional, street_name_pre_type, street_name, street_name_post_type, street_name_post_directional, subaddress_type, subaddress_identifier, occupancy_type, occupancy_identifier, building_name, place_name, state_name, zip_code, zip_plus_4, complete_address_number, complete_street_name, complete_subaddress, complete_place_name, delivery_address_without_unit, delivery_address_with_unit, address_number_parity, parsed_address))
            except Exception as e:
                print('Error: {}'.format(e))
                print('INSERT INTO ' + output_table + ' (fkid, parser_had_issues, modified_by_cpc, custom_parsed, parser_message, address_to_parse, alesco_address_type, parserator_tag, address_number, address_number_suffix, street_name_pre_directional, street_name_pre_type, street_name, street_name_post_type, street_name_post_directional, subaddress_type, subaddress_identifier, occupancy_type, occupancy_identifier, building_name, place_name, state_name, zip_code, zip_plus_4, complete_address_number, complete_street_name, complete_subaddress, complete_place_name, delivery_address_without_unit, delivery_address_with_unit, address_number_parity, parsed_address) VALUES ({}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {})'.format(fk_id, parser_had_issues, modified_by_cpc, custom_parsed, parser_message, full_address_input, alesco_address_type, parserator_tag, validated_address_number, address_number_suffix, street_name_pre_directional, street_name_pre_type, street_name, street_name_post_type, street_name_post_directional, subaddress_type, subaddress_identifier, occupancy_type, occupancy_identifier, building_name, place_name, state_name, zip_code, zip_plus_4, complete_address_number, complete_street_name, complete_subaddress, complete_place_name, delivery_address_without_unit, delivery_address_with_unit, address_number_parity, parsed_address))
                exit()
            finally:
                if vverbose:
                    print('--------- INSERT complete ----------------------')

        #------------------------------------------------------------------------------
        # Commit the changes every so often
        #------------------------------------------------------------------------------
        commitCounter += 1
        commitFrequency = 10000
        if (commitCounter % commitFrequency == 0):
            connection.commit()

            t2 = datetime.now()
            tdelta = t2 - t1

            if verbose:
                print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
                print('                                                                               ')
                print('** Script processing time **')
                print('                                                                               ')
                print('Counter at ' + str(counter) + ' of ' + str(selected_input_record_count))
                print('Committing at ' + str(commitCounter))
                print('Delta: {0} per {1}'.format(tdelta, commitFrequency))

                current = datetime.now()
                elapsed = current - start
                print('Script start time: {0}'.format(start))
                print('Script current time:   {0}'.format(current))
                print('Script run time:               {0}'.format(elapsed))

                # seconds per records
                elapsed_secs = elapsed.seconds
                recs_per_secs = counter / elapsed_secs
                print('recs_per_secs: {:,.0f}'.format(recs_per_secs))

                # Total seconds est
                est_total_secs = selected_input_record_count / recs_per_secs
                print('est_total_secs: {:,.0f}'.format(est_total_secs))

                # Seconds remaining
                est_remain_secs = est_total_secs - elapsed_secs
                print('est_remain_secs: {:,.0f}'.format(est_remain_secs))
                if (est_remain_secs > 60):
                    est_remain_mins = est_remain_secs / 60
                    print('est_remain_mins: {:,.0f}'.format(est_remain_mins))

            t1 = t2 #reset t1 for the next iteration
            commitCounter = 0
    connection.commit()

    # ######################################################################################
    # Join input table
    # ######################################################################################
    print('---------------------------')
    print('Joining input table to output table')
    print('---------------------------')
    # join and sort on fk_id field to preserve order from initial input step

    # hardcode and try the join first
    sql = 'SELECT ' + output_table + '.*, ' + address_table + '.*'
    sql += ' FROM ' + output_table + ', ' + address_table
    sql += ' WHERE ' + output_table + '.fk_id = ' + address_table + '.' + primary_key
    sql += ' ORDER BY ' + address_table + '.' + primary_key
    # sql += ' WHERE ' + output_table + '.fk_id = ' + address_table + '.' + primary_key
    # sql += ' ORDER BY ' + address_table + '.' + primary_key
    print('sql:"{}"'.format(sql))
    cur.execute(sql)

    sql = 'DROP TABLE IF EXISTS xxxtempsort'
    cur.execute(sql)
    sql = 'CREATE TABLE xxxtempsort AS SELECT * FROM ' + output_table
    cur.execute(sql)
    sql = 'DROP TABLE ' + output_table
    cur.execute(sql)
    sql = 'CREATE TABLE ' + output_table + ' AS '
    sql += ' SELECT ' + address_table + '.*, xxxtempsort.* '
    sql += ' FROM xxxtempsort, ' + address_table
    sql += ' WHERE xxxtempsort.fk_id = ' + address_table + '.' + primary_key
    #sql += ' ORDER BY ' + address_table + '.' + primary_key
    sql += ' ORDER BY street_name, street_name_pre_type, street_name_pre_directional, street_name_post_type, street_name_post_directional, address_number, address_number_suffix, subaddress_type, subaddress_identifier'

    print('sql:"{}"'.format(sql))
    cur.execute(sql)
    sql = 'DROP TABLE xxxtempsort'
    cur.execute(sql)
    sql = 'ALTER TABLE ' + output_table + ' DROP COLUMN fk_id'
    cur.execute(sql)

    # sql = 'DROP TABLE IF EXISTS xxxtempsort'
    # print('sql:"{}"'.format(sql))
    # cur.execute(sql)

    # sql = 'CREATE TABLE xxxtempsort as select * from ' + output_table
    # sql += ' ORDER BY street_name, street_name_pre_type, street_name_pre_directional, street_name_post_type, street_name_post_directional, address_number, address_number_suffix, subaddress_type, subaddress_identifier'
    # print('sql:"{}"'.format(sql))
    # cur.execute(sql)

    # sql = 'drop table ' + output_table
    # print('sql:"{}"'.format(sql))
    # cur.execute(sql)

    # sql = 'create table ' + output_table + ' as select * from xxxtempsort'
    # print('sql:"{}"'.format(sql))
    # cur.execute(sql)

    # sql = 'drop table xxxtempsort'
    # print('sql:"{}"'.format(sql))
    # cur.execute(sql)

    connection.commit()


    #------------------------------------------------------------------------------
    # Add SFGIS primary key
    #------------------------------------------------------------------------------
    # Let the program fail if the input table already has this column
    # Use this key for sorting for the remainder of the GAP steps. The final GAP step may drop this column.
    #sql = 'ALTER TABLE ' + output_table + ' ADD COLUMN sfgisgapid SERIAL NOT NULL PRIMARY KEY;'
    # sql = 'ALTER TABLE ' + output_table + ' ADD COLUMN sfgisgapid SERIAL;'
    # print('sql: ' + sql)
    # cur.execute(sql)
    # connection.commit()

    #------------------------------------------------------------------------------
    # Add counter field
    #------------------------------------------------------------------------------
    sql = 'ALTER TABLE ' + output_table + ' ADD COLUMN counter SERIAL;'
    print('sql: ' + sql)
    cur.execute(sql)
    connection.commit()



    # ######################################################################################
    print('*********************************************')
    print('Done processing records')
    print('*********************************************')
    #exit()

    # ######################################################################################
    # Get report info about destination table
    # ######################################################################################

    # Get record count
    sql = 'SELECT COUNT(*) AS count FROM ' + output_table
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    output_record_count = row[0]

    # Get sample record
    sql = 'SELECT * FROM ' + output_table
    sql += ' LIMIT 1'
    cur.execute(sql)

    # Echo input column names
    output_column_list = [desc[0] for desc in cur.description]
    if vverbose:
        print('Destination Columns: {0}'.format(output_column_list))

    rows = cur.fetchall()
    for row in rows:
        first_output_row = row
        if vverbose:
            print('Destination Row: {0}'.format(row))

finally:
    foo = 'foobar'

# ######################################################################################
# Calculate run time 
# ######################################################################################
# Calculate processing time, time per recs ,  recs per time
end = datetime.now()
elapased = end - start
# Convert elapsed time into seconds
elapased_seconds = sfgis_toolbox.get_sec(format(elapased))
# Time per 1 record
if counter:
    seconds_per_record = elapased_seconds / counter
else:
    seconds_per_record = 0
# Rec/sec
records_per_second = counter / elapased_seconds

# ######################################################################################
# Save report
# ######################################################################################
wb = Workbook()

sheet_index = 0

# Record Count Sheet
ws1 = wb.create_sheet('Record Count Summary', sheet_index)
row = 0

# Script name
row += 1
col = 1
ws1.cell(row=row, column=col, value='Script name:')
col = 2
ws1.cell(row=row, column=col, value=format(sys.argv[0]))

# Script start time
row += 1
col = 1
ws1.cell(row=row, column=col, value='Script start time:')
col = 2
ws1.cell(row=row, column=col, value=format(start))

# Script end time
row += 1
col = 1
ws1.cell(row=row, column=col, value='Script end time:')
col = 2
ws1.cell(row=row, column=col, value=format(end))

# Script run time
row += 1
col = 1
ws1.cell(row=row, column=col, value='Script run time:')
col = 2
ws1.cell(row=row, column=col, value=format(elapased))

# Break
row += 1

# Input Table
row += 1
col = 1
ws1.cell(row=row, column=col, value='Input Table of Addresses:')
col = 2
ws1.cell(row=row, column=col, value=format(address_table))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Input Record Count:')
col = 2
ws1.cell(row=row, column=col, value=format(input_record_count))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Input Record Limit:')
col = 2
ws1.cell(row=row, column=col, value=format(limit_message))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Input Records Processed:')
col = 2
ws1.cell(row=row, column=col, value=format(counter))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Input Records with Parsing Issue:')
col = 2
ws1.cell(row=row, column=col, value=format(parser_issue_count))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Input Records Parsed with Custom Logic:')
col = 2
ws1.cell(row=row, column=col, value=format(custom_parse_count))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Records skipped:')
col = 2
ws1.cell(row=row, column=col, value=format(record_skip_count))

row += 1
col = 1
ws1.cell(row=row, column=col, value='Records modified by CDC "imprefect_addresses" routine:')
col = 2
ws1.cell(row=row, column=col, value=format(modified_by_cpc_count))




# Record Processing Time
row += 1
col = 1
ws1.cell(row=row, column=col, value='Record processing time:')
col = 2
ws1.cell(row=row, column=col, value='Seconds to process {0} records: {1:,.4f}'.format(counter, elapased_seconds))

# Break
row += 1

# Processing Time Estimates
row += 1
col = 1
ws1.cell(row=row, column=col, value='Processing time estimates:')
col = 2
ws1.cell(row=row, column=col, value=format(elapased))
row += 1 # Break
row += 1
col = 1
ws1.cell(row=row, column=col, value='Seconds to process 1 record:')
col = 2
ws1.cell(row=row, column=col, value='{:,.4f}'.format(seconds_per_record))
row += 1
col = 1
ws1.cell(row=row, column=col, value='Seconds to process 1000 records:')
col = 2
ws1.cell(row=row, column=col, value='{:,.2f}'.format(seconds_per_record * 1000))
row += 1
col = 1
ws1.cell(row=row, column=col, value='Minutes to process 1,000,000 records:')
col = 2
ws1.cell(row=row, column=col, value='{:,.1f}'.format(seconds_per_record * 1000000 / 60))
row += 1 # Break
row += 1
col = 1
ws1.cell(row=row, column=col, value='Records processed per second:')
col = 2
ws1.cell(row=row, column=col, value='{:,.0f}'.format(records_per_second))
row += 1
col = 1
ws1.cell(row=row, column=col, value='Records processed per minute:')
col = 2
ws1.cell(row=row, column=col, value='{:,.0f}'.format(records_per_second * 60))
row += 1
col = 1
ws1.cell(row=row, column=col, value='Records processed per hour:')
col = 2
ws1.cell(row=row, column=col, value='{:,.0f}'.format(records_per_second * 60 * 60))

# Break
row += 1

# Input Records
row += 1
col = 1
ws1.cell(row=row, column=col, value='Input Records Processed:')
col = 2
ws1.cell(row=row, column=col, value=str(counter))

# Break
row += 1

# Input Columns
row += 1
col = 1
ws1.cell(row=row, column=col, value='Input Columns:')
col = 2
ws1.cell(row=row, column=col, value=format(input_column_list))

# Break
row += 1

# First Input Row
row += 1
col = 1
ws1.cell(row=row, column=col, value='First Input Row:')
col = 2
ws1.cell(row=row, column=col, value=format(first_input_row))

# Break
row += 1

# First Address to be Parsed
row += 1
col = 1
ws1.cell(row=row, column=col, value='First Address to be Parsed:')
col = 2
ws1.cell(row=row, column=col, value=format(first_address_parsed))

# Break
row += 1

# First Input Row
row += 1
col = 1
ws1.cell(row=row, column=col, value='First Parsed Results:')
col = 2
ws1.cell(row=row, column=col, value=format(first_parsed_address))

# Break
row += 1

# First Input Row
row += 1
col = 1
ws1.cell(row=row, column=col, value='Output Table of Parsed Addresses:')
col = 2
ws1.cell(row=row, column=col, value=format(output_table))

# Break
row += 1

# First Input Row
row += 1
col = 1
ws1.cell(row=row, column=col, value='Output Record Count:')
col = 2
ws1.cell(row=row, column=col, value=format(output_record_count))

# Break
row += 1

# First Input Row
row += 1
col = 1
ws1.cell(row=row, column=col, value='Output Columns:')
col = 2
ws1.cell(row=row, column=col, value=format(output_column_list))

# Break
row += 1

# First Input Row
row += 1
col = 1
ws1.cell(row=row, column=col, value='First Output Row:')
col = 2
ws1.cell(row=row, column=col, value=format(first_output_row))

# Break
row += 1

# End of sheet
row += 1
col = 1
ws1.cell(row=row, column=col, value='End of sheet')

# TODO: Turn into a function and call for all sheets
# Adjust column widths
# See also https://stackoverflow.com/questions/13197574/openpyxl-adjust-column-width-size
dims = {}
for row in ws1.rows:
    for cell in row:
        if cell.value:
            dims[cell.column] = max((dims.get(cell.column, 0), len(cell.value)))
for col, value in dims.items():
    ws1.column_dimensions[col].width = value

def write_field_summary(field_name, sheet_index, wb, cur):
    sheet_index += 1
    ws = wb.create_sheet(field_name + ' Summary')
    #ws = wb.create_sheet(field_name + ' Summary', sheet_index)
    row = 0

    # Field name
    # Query the field name
    sql = 'SELECT ' + field_name + ', COUNT(' + field_name + ') AS COUNT FROM ' + output_table + ' GROUP BY ' + field_name + ' ORDER BY COUNT DESC'

    cur.execute(sql)

    # Echo the field name
    row += 1
    col = 1
    ws.cell(row=row, column=col, value='Field:')
    col = 2
    ws.cell(row=row, column=col, value=format(field_name))

    # Break
    row += 1

    # Write Header
    row += 1
    col = 1
    ws.cell(row=row, column=col, value='VALUE')
    col = 2
    ws.cell(row=row, column=col, value='COUNT')

    # Write Rows
    rows = cur.fetchall()
    for r in rows:
        row += 1
        col = 1
        ws.cell(row=row, column=col, value=format(r[0]))
        col = 2
        ws.cell(row=row, column=col, value=format(r[1]))

write_field_summary('parserator_tag', sheet_index, wb, cur)
write_field_summary('parser_had_issues', sheet_index, wb, cur)
write_field_summary('modified_by_cpc', sheet_index, wb, cur)
write_field_summary('custom_parsed', sheet_index, wb, cur)
write_field_summary('parser_message', sheet_index, wb, cur)
write_field_summary('alesco_address_type', sheet_index, wb, cur)
write_field_summary('address_number', sheet_index, wb, cur)
write_field_summary('address_number_suffix', sheet_index, wb, cur)
write_field_summary('street_name_pre_directional', sheet_index, wb, cur)
write_field_summary('street_name_pre_type', sheet_index, wb, cur)
write_field_summary('street_name', sheet_index, wb, cur)
write_field_summary('street_name_post_type', sheet_index, wb, cur)
write_field_summary('street_name_post_directional', sheet_index, wb, cur)
write_field_summary('complete_street_name', sheet_index, wb, cur)
write_field_summary('subaddress_type', sheet_index, wb, cur)
write_field_summary('subaddress_identifier', sheet_index, wb, cur)
write_field_summary('occupancy_type', sheet_index, wb, cur)
write_field_summary('occupancy_identifier', sheet_index, wb, cur)
write_field_summary('building_name', sheet_index, wb, cur)
write_field_summary('place_name', sheet_index, wb, cur)
write_field_summary('state_name', sheet_index, wb, cur)
write_field_summary('zip_code', sheet_index, wb, cur)
write_field_summary('address_number_parity', sheet_index, wb, cur)

# Save report
wb.save(output_report_file)

# Close database connection
if connection:
    connection.close()

# ######################################################################################
# Display results
# ######################################################################################

print('                                                                               ')
print('===============================================================================')
print('                                                                               ')
print(sys.argv[0])
print('                                                                               ')
# print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
# print('                                                                               ')
# print ('args: {0}'.format(args)) #TODO Filter out password and/or all odbc values before outputing
# print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('Input Table of Addresses: {0}'.format(address_table))
print('Input Record Count: {0}'.format(input_record_count))
print('Input Record Limit: ' + limit_message)
print('Input Records Processed: ' + (str(counter)))
print('Input Records with Parsing Issue: ' + (str(parser_issue_count)))
print('Input Records Parsed with Custom Logic: ' + (str(custom_parse_count)))
#print('Number of rows where street_name found in EAS: ' + (str(found_street_name_count)))
#print('Number of rows where street_name not found in EAS: ' + (str(not_found_street_name_count)))
print('Records skipped: {}'.format(record_skip_count))
print('Records modified by CPC "imperfect_addresses" routine: {}'.format(modified_by_cpc_count))

print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('** Input Columns **')
print('                                                                               ')
print(format(input_column_list))
print('                                                                               ')
print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
print('                                                                               ')
print('** First Input Row **')
print('                                                                               ')
print(format(first_input_row))
print('                                                                               ')
print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
print('                                                                               ')
print(' ** First Address to be Parsed **')
print('                                                                               ')
print(format(first_address_parsed))
print('                                                                               ')
print(' ** First Parsed Results **')
print('                                                                               ')
print(format(first_parsed_address))
print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('** Output Table of Parsed Addresses **')
print('                                                                               ')
print(format(output_table))
print('                                                                               ')
print('Output Record Count: {0}'.format(output_record_count))
print('                                                                               ')
print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
print('                                                                               ')
print('** Output Columns **')
print('                                                                               ')
print(format(output_column_list))
print('                                                                               ')
print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
print('                                                                               ')
print('** First Output Row **')
print('                                                                               ')
print(format(first_output_row))
print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('** Output Report File **')
print('                                                                               ')
print(format(output_report_file))
print('                                                                               ')
print('** Output Report Sheets **')
print('                                                                               ')
for sheet in wb:
    print(format(sheet.title))
print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('** Processing time estimates **')
print('                                                                               ')
print('Seconds to process 1 record: {:,.4f}'.format(seconds_per_record))
print('Seconds to process 1000 records: {:,.2f}'.format(seconds_per_record * 1000))
print('Minutes to process 1,000,000 records: {:,.1f}'.format(seconds_per_record * 1000000 / 60))
print('                                                                               ')
print('Records processed per second: {:,.0f}'.format(records_per_second))
print('Records processed per minute: {:,.0f}'.format(records_per_second * 60))
print('Records processed per hour: {:,.0f}'.format(records_per_second * 60 * 60))
print('                                                                               ')
print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
print('                                                                               ')
print('** Record processing time **')
print('                                                                               ')
print('Seconds to process {0} records: {1:,.4f}'.format(counter, elapased_seconds))
print('                                                                               ')

sfgis_toolbox.print_run_time(start)

print('*******************************************************************************')
print('                                                                               ')
print('End of script ' + sys.argv[0])
print('                                                                               ')
