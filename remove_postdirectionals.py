print('*******************************************************************************')
print('*******************************************************************************')
print('*******************************************************************************')
print('Begin script.')

# ######################################################################################
# Import modules
# ######################################################################################
import argparse # Parse CLI args
from datetime import datetime
import openpyxl # Library used to create report file
from openpyxl import Workbook
import psycopg2 # PostgreSQL driver
import re       #regular expressions
import sfgis_toolbox # Reusable routines by SFGIS
import sys
import usaddress # Parserator 'usaddress' parsing module # See https://parserator.datamade.us/api-docs

# ######################################################################################
# Start script timer
# ######################################################################################
start = datetime.now()
print(sys.argv[0] + ' start: {0}'.format(start))

# ######################################################################################
# Read command line arguments
# ######################################################################################
argparser=argparse.ArgumentParser()

# Required database connection args
argparser.add_argument('--odbc_server', help='Database Server', type=str)
argparser.add_argument('--odbc_port',  help='Database Port', type=str)
argparser.add_argument('--odbc_database', help='Database Name', type=str)
argparser.add_argument('--odbc_uid', help='User Name', type=str)
argparser.add_argument('--odbc_pwd', help='User Password', type=str)

# Required args
argparser.add_argument('--address_table', help='Input address table', type=str)
argparser.add_argument('--output_table', help='Output address table (with P.O. Boxes removed)', type=str)
argparser.add_argument('--output_report_file', help='Output report file (CSV or XLSX)', type=str)

# Optional args

# Parse args
args=argparser.parse_args()

# Store args as local variables
odbc_server = args.odbc_server
odbc_port = args.odbc_port
odbc_database = args.odbc_database
odbc_uid = args.odbc_uid
odbc_pwd = args.odbc_pwd
address_table = args.address_table
output_table = args.output_table
output_report_file = args.output_report_file

# Print usage and halt if missing any required arguments
if any(elem is None for elem in [odbc_server, odbc_port, odbc_database, odbc_uid, odbc_pwd, address_table, output_table, output_report_file]):
    print(argparser.format_help())
    exit()

# ######################################################################################
# Define database connection info
# ######################################################################################
odbc_driver = '{psqlodbc}'

connection = None

# ######################################################################################
# Connect to database
# ######################################################################################

try:
    connection = psycopg2.connect(host=odbc_server, database=odbc_database, user=odbc_uid, password=odbc_pwd)

    cur = connection.cursor()

    # ######################################################################################
    # Drop output table
    # ######################################################################################
    sql = 'DROP TABLE IF EXISTS ' + output_table
    cur.execute(sql)
    connection.commit()

    print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
    print('Input/Output Counts')
    print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
    # ######################################################################################
    # Get total record count
    # ######################################################################################
    sql = 'SELECT COUNT(*) AS count FROM ' + address_table
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    input_record_count = row[0]
    print('input_record_count: {0}'.format(input_record_count))

    # ######################################################################################
    # Save copy of table without street names that have a post directional
    # Note: we exclude both the streets with the post directional as well as streets with same
    # name without the post directional
    # ######################################################################################

    # See also 
    #       https://bitbucket.org/sfgovdt/eas/issues/168
    #       "Provide for street names that have a post directional"
    # 
    #       R:\Tec\Division\EnterpriseOperations\ApplicationsAndDatabase\EnterpriseApplications\SfEnterpriseGisProgram\Application\Eas\_Task\2015_2016\20150806_168_StNamePostDirectional
    #
    # Exclude these:
    # "25TH AVE"
    # "BATTERY RD"
    # "BUENA VISTA AVE"
    # "BURNETT AVE"
    # "CONSERVATORY DR"
    # "EL DORADO ST"
    # "FIRST DR"
    # "FORT FUNSTON RD"
    # "LAKE MERCED HILL ST"
    # "MISSION BAY BLVD"
    # "STOW LAKE DR"
    # "WILLARD ST"

    sql = 'CREATE TABLE ' + output_table + ' AS SELECT * FROM ' + address_table
    sql += " WHERE NOT ("
    #sql += " WHERE ("
    sql += " ( 1=0 )"
    sql += " OR ( street_name LIKE '25TH' AND street_name_post_type LIKE 'AVE' ) "
    sql += " OR ( street_name LIKE 'BATTERY' AND street_name_post_type LIKE 'RD' ) " # Note: None in State of CA 2018
    sql += " OR ( street_name LIKE 'BUENA VISTA' AND street_name_post_type LIKE 'AVE' ) "
    sql += " OR ( street_name LIKE 'BURNETT' AND street_name_post_type LIKE 'AVE' ) "
    sql += " OR ( street_name LIKE 'CONSERVATORY' AND street_name_post_type LIKE 'DR' ) " # Note: None in State of CA 2018
    sql += " OR ( street_name LIKE 'EL DORADO' AND street_name_post_type LIKE 'ST' ) " # Note: None in State of CA 2018
    sql += " OR ( street_name LIKE 'FIRST' AND street_name_post_type LIKE 'DR' ) " # Note: None in State of CA 2018
    sql += " OR ( street_name LIKE 'FORT FUNSTON' AND street_name_post_type LIKE 'RD' ) " # Note: None in State of CA 2018
    sql += " OR ( street_name LIKE 'LAKE MERCED HILL') "
    #sql += " OR ( street_name LIKE 'LAKE MERCED HILL' AND street_name_post_type LIKE 'ST' ) "
    sql += " OR ( street_name LIKE 'MISSION BAY' AND street_name_post_type LIKE 'BLVD' ) "
    sql += " OR ( street_name LIKE 'STOW LAKE' AND street_name_post_type LIKE 'DR' ) "
    sql += " OR ( street_name LIKE 'WILLARD' AND street_name_post_type LIKE 'ST' ) "
    sql += " ) "
    cur.execute(sql)
    connection.commit()
    # Echo record count
    sql = 'SELECT COUNT(*) AS count FROM ' + output_table
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    output_table_count = row[0]
    print('output_table_count: {0}'.format(output_table_count)) # 408435

    # ######################################################################################
    # Calculate and report the difference in records after removing P.O. Boxes
    # ######################################################################################
    records_excluded = input_record_count - output_table_count
    print('Record excluded (input-output): {0}'.format(records_excluded)) # 18494


finally:
    status = 'complete'

# ######################################################################################
# Calculate run time 
# ######################################################################################
# Calculate processing time, time per recs ,  recs per time
end = datetime.now()
elapased = end - start
# Convert elapsed time into seconds
elapased_seconds = sfgis_toolbox.get_sec(format(elapased))

# Close database connection
if connection:
    connection.close()

# ######################################################################################
# Display results
# ######################################################################################

print('                                                                               ')
print('===============================================================================')
print('                                                                               ')
print(sys.argv[0])
print('                                                                               ')
# print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
# print('                                                                               ')
# print ('args: {0}'.format(args)) #TODO Filter out password and/or all odbc values before outputing
# print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('Input Table of Addresses: {0}'.format(address_table))
print('Input Record Count: {0}'.format(input_record_count))
print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')

sfgis_toolbox.print_run_time(start)

print('*******************************************************************************')
print('                                                                               ')
print('End of script ' + sys.argv[0])
print('                                                                               ')
