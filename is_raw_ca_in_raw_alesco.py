print('*******************************************************************************')
print('*******************************************************************************')
print('*******************************************************************************')
print('Begin script.')

# ######################################################################################
# Import modules
# ######################################################################################
import argparse # Parse CLI args
from datetime import datetime
import openpyxl # Library used to create report file
from openpyxl import Workbook
import psycopg2 # PostgreSQL driver
import re       #regular expressions
import sfgis_toolbox # Reusable routines by SFGIS
import sys

# ######################################################################################
# Start script timer
# ######################################################################################
start = datetime.now()
print(sys.argv[0] + ' start: {0}'.format(start))

# ######################################################################################
# Read command line arguments
# ######################################################################################
argparser=argparse.ArgumentParser()

# Required database connection args
argparser.add_argument('--odbc_server', help='Database Server', type=str)
argparser.add_argument('--odbc_port',  help='Database Port', type=str)
argparser.add_argument('--odbc_database', help='Database Name', type=str)
argparser.add_argument('--odbc_uid', help='User Name', type=str)
argparser.add_argument('--odbc_pwd', help='User Password', type=str)

# Input table
argparser.add_argument('--input_ca_table', help='Input address table', type=str)

# TODO replace hard-coded field names with args
# Required address args
# argparser.add_argument('--input_address_number', help='Input column containing address_number', type=str)
# argparser.add_argument('--input_street_name', help='Input column containing street name', type=str)
# argparser.add_argument('--input_street_post_type', help='Input column containing street post type', type=str)
# argparser.add_argument('--primary_key', help='Primary key of th input table', type=str)

# Optional address args
# argparser.add_argument('--address_number_suffix', help='Optional. Input column containing address_number_suffix', type=str, default='')
# argparser.add_argument('--unit_number', help='Optional. Input column containing unit_number', type=str, default='')
# argparser.add_argument('--zipcode', help='Optional. Input column containing zipcode', type=str, default='')

# Compare Table
argparser.add_argument('--input_alesco_table', help='Input alsco table', type=str)

# TODO: Report file and sheetname
# argparser.add_argument('--report_file_name', help='Report file (XLSX)', type=str)
# argparser.add_argument('--report_sheet_name', help='Report sheet name. Appends with digit if sheet already exists.', type=str)

# Input record limit
argparser.add_argument('--limit', help='Limit records. Set to -1 for all records.', type=int, default=0)

# Parse args
args=argparser.parse_args()

# Store args as local variables
odbc_server = args.odbc_server
odbc_port = args.odbc_port
odbc_database = args.odbc_database
odbc_uid = args.odbc_uid
odbc_pwd = args.odbc_pwd
input_ca_table = args.input_ca_table
input_alesco_table = args.input_alesco_table
# input_address_number = args.input_address_number
# input_street_name = args.input_street_name
# input_street_post_type = args.input_street_post_type
# primary_key = args.primary_key
# address_number_suffix = args.address_number_suffix
# unit_number = args.unit_number
# zipcode = args.zipcode
# report_file_name = args.report_file_name
# report_sheet_name = args.report_sheet_name
limit = args.limit

# Print usage and halt if missing any required arguments
if any(elem is None for elem in [odbc_server, odbc_port, odbc_database, odbc_uid, odbc_pwd, input_ca_table, input_alesco_table]):
    print(argparser.format_help())
    exit()

# ######################################################################################
# Define database connection info
# ######################################################################################
odbc_driver = '{psqlodbc}'

connection = None

# ######################################################################################
# Define verbosity and limits of output
# ######################################################################################
verbose = 1
vverbose = 1

# Define query limits
if args.limit == -1:
    limit_source = ''
    limit_message = 'All records'
elif args.limit == 0:
    limit_source = ' LIMIT 0'
    limit_message = 'No records'
elif args.limit == 1:
    limit_source = ' LIMIT ' + format(limit)
    limit_message = '1 record'
elif args.limit > 1:
    limit_source = ' LIMIT ' + format(limit)
    limit_message = format(limit) + ' records'
else:
    print('Error. Invalid value for limit: {0}'.format(limit))
    exit()

# ######################################################################################
# Init record processing counter and other report variables
# ######################################################################################
counter = 0 # track input record count

print('Counter: ' + (str(counter)))
commitCounter = 0 # track iterations of records between commits
t1 = datetime.now() # track execution of the script

# Init report variables: sample data from first record
# (Init to blank in case the query returns 0 records)
input_record_count = 0
input_column_list = []
first_input_row = []

addressess_found = 0
addressess_not_found = 0
addressess_with_duplicates = 0

eas_baseid_index = -1
eas_subid_index = -1
block_index = -1
lot_index = -1

count_is_in_eas = 0
count_not_in_eas = 0

compare_record_count = 0
compare_column_list = []
first_compare_row = []

# ######################################################################################
# Connect to database
# ######################################################################################
try:
    connection = psycopg2.connect(host=odbc_server, database=odbc_database, user=odbc_uid, password=odbc_pwd)

    cur = connection.cursor()

    # ######################################################################################
    # Create report fields 
    # ######################################################################################
    create_report_field = True

    if create_report_field:

        print('BEGIN: Create report fields')

        # Field is_in_alesco
        sql = 'ALTER TABLE ' + input_ca_table + ' DROP COLUMN IF EXISTS in_alesco'
        print(sql)
        cur.execute(sql)
        connection.commit()
        sql = 'ALTER TABLE ' + input_ca_table + ' ADD COLUMN in_alesco INTEGER NULL DEFAULT -1'
        print(sql)
        cur.execute(sql)
        connection.commit()

        print('END: Create report fields')

    # ######################################################################################
    # Create index on the query fields
    # ######################################################################################
    create_index = True

    if create_index:
        print('BEGIN: Create index fields')

        #sql = 'CREATE INDEX ON ' + input_ca_table + ' (id)'
        #cur.execute(sql)
        #sql = 'CREATE INDEX ON ' + input_ca_table + ' (sfgisgapid)'
        #cur.execute(sql)

        #sql = 'ALTER TABLE ' + input_ca_table + ' ADD CONSTRAINT PK' + input_ca_table + ' PRIMARY KEY USING INDEX sfgisgapid'

        #sql = 'CREATE INDEX ON ' + input_ca_table + ' (id)'
        #cur.execute(sql)


        #['opt_endrs', 'slug', 'address', 'city', 'state', 'zip', 'crrt', 'routestart', 'dpbc', 'walkseq', 'deltype', 'addrstat2', 'sfgisgapid']

        sql = 'CREATE INDEX ON ' + input_alesco_table + ' (address)'
        cur.execute(sql)
        sql = 'CREATE INDEX ON ' + input_alesco_table + ' (city)'
        cur.execute(sql)
        sql = 'CREATE INDEX ON ' + input_alesco_table + ' (state)'
        cur.execute(sql)

        connection.commit()

        print('END: Create index fields')

    # ######################################################################################
    # Get record count
    # ######################################################################################
    sql = 'SELECT COUNT(*) AS count FROM ' + input_ca_table
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    input_record_count = row[0]
    print('input_record_count: {0}'.format(input_record_count))

    # ######################################################################################
    # Get source records
    # ######################################################################################
    #['pbkey', 'add_number', 'streetname', 'unit_des', 'unit_num', 'city', 'state', 'zipcode', 'type', 'fips', 'lat', 'lon', 'sfgisgapid']
    sql = 'SELECT sfgisgapid '
    sql += ' , add_number'
    sql += ' , streetname'
    sql += ' , unit_des'
    sql += ' , unit_num'
    sql += ' , city'
    sql += ' , state'
    sql += ' , zipcode'
    sql += ' FROM ' + input_ca_table
    #sql += ' ORDER BY counter '
    #sql += ' ORDER BY sfgisgapid '
    sql += limit_source
    cur.execute(sql)

    # Echo input column names
    input_column_list = [desc[0] for desc in cur.description]
    if vverbose:
        print('Source Columns: {0}'.format(input_column_list))

    rows = cur.fetchall()
    for row in rows:
        counter+=1
        if verbose:
            print('---------------------------------')
            print('{}. Source Row: {}'.format(counter, row))

        # ====================================================
        # Init vars
        # ====================================================

        if counter == 1:
            first_input_row = row
            if vverbose:
                print('First Source Row: {0}'.format(first_input_row))

        # ######################################################################################
        # TODO: Replace hard-coded field names with args
        # ######################################################################################
        primary_key = 'sfgisgapid'

        #['pbkey', 'add_number', 'streetname', 'unit_des', 'unit_num', 'city', 'state', 'zipcode', 'type', 'fips', 'lat', 'lon', 'sfgisgapid']

        input_address_number = 'add_number'
        input_street_name = 'streetname'
        input_unit_type = 'unit_des'
        input_unit_identifier = 'unit_num'
        input_city = 'city'


        # ######################################################################################
        # Get address from input row
        # ######################################################################################
        # Get ID
        pk_index = input_column_list.index(primary_key)
        input_id =row[pk_index]
        if int(input_id) > 0:
            if vverbose:
                print('id is {0}'.format(input_id))
        else:
            print('Error. Did not find record id in ' + input_ca_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()

        # ===================================
        # Get Input Address Values
        # TODO: Run this logic on first record only.
        # ===================================
        if input_address_number in input_column_list:
            address_number_index = input_column_list.index(input_address_number)
        else:
            print('Error. Did not find column ' + input_address_number + ' in ' + input_ca_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()
        address_number_value = row[address_number_index]

        if input_street_name in input_column_list:
            street_name_index = input_column_list.index(input_street_name)
        else:
            print('Error. Did not find column ' + input_street_name + ' in ' + input_ca_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()
        street_name_value = row[street_name_index]

        if input_unit_type in input_column_list:
            unit_type_index = input_column_list.index(input_unit_type)
        else:
            print('Error. Did not find column ' + input_unit_type + ' in ' + input_ca_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()
        unit_type_value = row[unit_type_index]

        if input_unit_identifier in input_column_list:
            unit_identifier_index = input_column_list.index(input_unit_identifier)
        else:
            print('Error. Did not find column ' + input_unit_identifier + ' in ' + input_ca_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()
        unit_identifier_value = row[unit_identifier_index]

        if input_city in input_column_list:
            city_index = input_column_list.index(input_city)
        else:
            print('Error. Did not find column ' + input_city + ' in ' + input_ca_table)
            print('Existing columns: {0}'.format(input_column_list))
            exit()
        city_value = row[city_index]

        print('**************')
        print('Input values:')
        print('**************')

        print('address_number_value: {}'.format(address_number_value))
        print('street_name_value: {}'.format(street_name_value))
        print('unit_type_value: {}'.format(unit_type_value))
        print('unit_identifier_value: {}'.format(unit_identifier_value))
        print('city_value: {}'.format(city_value))

        # address_number_value
        # street_name_value
        # unit_type_value
        # unit_identifier_value
        # city_value

        # remove ' from street_name_value
        #"didn't".replace("'", "")
        street_name_value = street_name_value.replace("'", "")

        ca_address = address_number_value + ' ' + street_name_value + ' ' + unit_type_value + ' ' + unit_identifier_value + ' ' # Note: the trailing space is important and required because the Alesco address record ends with a space.

        # ######################################################################################
        # Answer Question: Is this address in Alesco, and if so get alesco id
        # ######################################################################################
        # ['opt_endrs', 'slug', 'address', 'city', 'state', 'zip', 'crrt', 'routestart', 'dpbc', 'walkseq', 'deltype', 'addrstat2', 'sfgisgapid']
        print('* * * * * * * *')
        sql = 'SELECT sfgisgapid, address, city'
        sql += ' FROM ' + input_alesco_table
        sql += ' WHERE 1=1 '
        sql += ' AND address = ' + "'" + ca_address + "'"
        sql += ' AND city = ' + "'"+city_value+"'"
        # sql += ' AND address_number = ' + str(address_number_value) # Int: no quotes
        # if address_number_suffix_value:
        #     sql += ' AND address_number_suffix = ' + "'"+address_number_suffix_value+"'"
        # sql += ' AND street_name = ' + "'"+street_name_value+"'"
        # sql += ' AND street_name_post_type = ' + "'"+street_post_type_value+"'"
        # # if street_post_directional_value:
        # #     sql += ' AND street_post_directional = ' + "'"+street_post_directional_value+"'"
        # if unit_number_value:
        #     sql += ' AND subaddress_identifier = ' + "'"+unit_number_value+"'"
        #     print('Query Alesco for the same unit')
        # else:
        #     print('Query Alesco for the same base')

        sql += ' LIMIT 1 '
        print('* * * * * * * *')

        if vverbose:
            print('sql: {}'.format(sql))
 
        cur.execute(sql)
        alesco_rows = cur.fetchall()
        in_alesco = 0
        if len(alesco_rows):
            in_alesco = 1
        for alesco_row in alesco_rows:
            if verbose:
                print('---------------------------------')
                print('Alesco Source Row: {}'.format(alesco_row))


        #------------------------------------------------------------------------------
        #  Update the record in the input table
        #------------------------------------------------------------------------------
        sql = 'UPDATE ' + input_ca_table
        sql += ' SET '
        sql += ' in_alesco = %s'
        sql += ' WHERE ' + primary_key + ' = ' + format(input_id)
        if vverbose:
            print('sql: {}'.format(sql))
        cur.execute(sql, [in_alesco])

        # ######################################################################################
        # ######################################################################################
        # ######################################################################################

        #------------------------------------------------------------------------------
        # Commit the changes every so often
        #------------------------------------------------------------------------------
        commitCounter += 1
        commitFrequency = 10000
        if (commitCounter % commitFrequency == 0):
            connection.commit()

            t2 = datetime.now()
            tdelta = t2 - t1

            if verbose:
                print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
                print('                                                                               ')
                print('** Script processing time **')
                print('                                                                               ')
                print('Counter at ' + str(counter) + ' of ' + str(input_record_count))
                print('Committing at ' + str(commitCounter))
                print('Delta: {0} per {1}'.format(tdelta, commitFrequency))

                current = datetime.now()
                elapsed = current - start
                print('Script start time: {0}'.format(start))
                print('Script current time:   {0}'.format(current))
                print('Script run time:               {0}'.format(elapsed))

                # seconds per records
                elapsed_secs = elapsed.seconds
                recs_per_secs = counter / elapsed_secs
                print('recs_per_secs: {:,.0f}'.format(recs_per_secs))

                # Total seconds est
                est_total_secs = input_record_count / recs_per_secs
                print('est_total_secs: {:,.0f}'.format(est_total_secs))

                # Seconds remaining
                est_remain_secs = est_total_secs - elapsed_secs
                print('est_remain_secs: {:,.0f}'.format(est_remain_secs))
                if (est_remain_secs > 60):
                    est_remain_mins = est_remain_secs / 60
                    print('est_remain_mins: {:,.0f}'.format(est_remain_mins))

            t1 = t2 #reset t1 for the next iteration
            commitCounter = 0

    # Final commit
    connection.commit()

    # ######################################################################################
    # Get report info about compare table
    # ######################################################################################

    # Get record count
    sql = 'SELECT COUNT(*) AS count FROM ' + input_alesco_table
    cur.execute(sql)
    rows = cur.fetchall()
    row = rows[0]
    compare_record_count = row[0]

    # Get sample record
    sql = 'SELECT * FROM ' + input_alesco_table
    sql += ' LIMIT 1'
    #print('Destination SQL: {0}'.format(sql))
    cur.execute(sql)

    # Echo input column names
    compare_column_list = [desc[0] for desc in cur.description]
    if vverbose:
        print('Comparison Columns: {0}'.format(compare_column_list))

    rows = cur.fetchall()
    for row in rows:
        first_compare_row = row
        if vverbose:
            print('Sample Comparison Row: {0}'.format(row))

finally:
    if vverbose:
        print('Done processing records.')

# ######################################################################################
# Calculate run time 
# ######################################################################################
# Calculate processing time, time per recs ,  recs per time
end = datetime.now()
elapased = end - start
# Convert elapsed time into seconds
elapased_seconds = sfgis_toolbox.get_sec(format(elapased))
# Time per 1 record
if counter:
    seconds_per_record = elapased_seconds / counter
else:
    seconds_per_record = 0
# Rec/sec
records_per_second = counter / elapased_seconds

# Close database connection
if connection:
    connection.close()

# ######################################################################################
# Display results
# ######################################################################################

print('                                                                               ')
print('===============================================================================')
print('                                                                               ')
print(sys.argv[0])
print('                                                                               ')
# print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
# print('                                                                               ')
# print ('args: {0}'.format(args)) #TODO Filter out password and/or all odbc values before outputing
# print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('Input Table of Addresses: {0}'.format(input_ca_table))
print('Input Record Count: {0}'.format(input_record_count))

print('Input Record Limit: ' + limit_message)

print('Input Records Processed: ' + (str(counter)))
print('                                                                               ')
# print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
# print('                                                                               ')
# print('** Input Columns **')
# print('                                                                               ')
# print(format(input_column_list))
# print('                                                                               ')
# print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
# print('                                                                               ')
# print('** First Input Row **')
# print('                                                                               ')
# print(format(first_input_row))
# print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('**  Table of Addresses to compare against **')
print('                                                                               ')
print(format(input_alesco_table))
print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('**  Results **')
print('                                                                               ')
print('Records in Compare Table: {0}'.format(compare_record_count))
print('                                                                               ')
print('                                                                               ')
#print('count_is_for_bulk_loader: {}'.format(count_is_for_bulk_loader))
print('                                                                               ')
print('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
print('                                                                               ')
print('** Processing time estimates **')
print('                                                                               ')
print('Seconds to process 1 record: {:,.4f}'.format(seconds_per_record))
print('Seconds to process 1000 records: {:,.2f}'.format(seconds_per_record * 1000))
print('Minutes to process 1,000,000 records: {:,.1f}'.format(seconds_per_record * 1000000 / 60))
print('                                                                               ')
print('Records processed per second: {:,.0f}'.format(records_per_second))
print('Records processed per minute: {:,.0f}'.format(records_per_second * 60))
print('Records processed per hour: {:,.0f}'.format(records_per_second * 60 * 60))
print('                                                                               ')
print('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
print('                                                                               ')
print('** Record processing time **')
print('                                                                               ')
print('Seconds to process {0} records: {1:,.4f}'.format(counter, elapased_seconds))
print('                                                                               ')

sfgis_toolbox.print_run_time(start)

print('*******************************************************************************')
print('                                                                               ')
print('End of script ' + sys.argv[0])
print('                                                                               ')
